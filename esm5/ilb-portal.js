import { __spread, __values, __extends } from 'tslib';
import { Injectable } from '@angular/core';
import { RequestMethod, Response } from '@angular/http';
import * as momentImported from 'moment';
import { decode } from 'urlsafe-base64';
import { Subject } from 'rxjs/Subject';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toArray';

var environment = {
    production: false,
    debug: {
        level: 'debug',
        apiCalls: '*',
        noBody: [
            '/api/auth/native/login',
            '/api/donations/history',
            '/api/sisense/sql'
        ]
    },
    services: {
        'profile.illuminations.bible': 'http://localhost:8001/api',
        'donation.illuminations.bible': 'http://localhost:8002/api',
        'score.seedconnect.com': 'http://localhost:8001/api/sisense'
    },
    stripe: {
        pk: 'pk_test_pjBShAQwejX0t8taYurjIj3S'
    },
    angularGoogleMaps: {
        apiKey: 'AIzaSyBL8yfwUb_VdHh3Mt7CKykCsoyRLSxPR9g'
    },
    trace: 'default environment',
    baseImageUrl: 'https://images.illuminations.bible/'
};
function errorToJsonString(err) {
    return JSON.stringify(err, ['message', 'arguments', 'type', 'name', 'stack']);
}
var BrowserService = /** @class */ (function () {
    function BrowserService() {
    }
    Object.defineProperty(BrowserService.prototype, "document", {
        get: function () {
            return document;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BrowserService.prototype, "window", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BrowserService.prototype, "Stripe", {
        get: function () {
            return ((window)).Stripe;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BrowserService.prototype, "sessionStorage", {
        get: function () {
            return (this.window || {}).sessionStorage || null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BrowserService.prototype, "localStorage", {
        get: function () {
            return (this.window || {}).localStorage || null;
        },
        enumerable: true,
        configurable: true
    });
    return BrowserService;
}());
BrowserService.decorators = [
    { type: Injectable },
];
BrowserService.ctorParameters = function () { return []; };
var GoogleAnalyticsService = /** @class */ (function () {
    function GoogleAnalyticsService(browserService) {
        this.browserService = browserService;
        this.logToGoogle = false;
        this.ga = this.browserService.window.ga;
        this.logToGoogle = environment.production;
    }
    GoogleAnalyticsService.prototype.error = function (description) {
        if (!this.logToGoogle) {
            return;
        }
        this.ga('send', 'exception', {
            exDescription: description
        });
    };
    GoogleAnalyticsService.prototype.pageView = function (newRoute) {
        if (!this.logToGoogle) {
            this.debug("[GoogleAnalytics] send:pageview:" + newRoute);
            return;
        }
        this.ga('send', 'pageview', newRoute);
    };
    GoogleAnalyticsService.prototype.languageSearchEvent = function (eventData) {
        var data;
        try {
            data = JSON.stringify({
                continent: eventData.continent,
                country: eventData.country,
                language: eventData.language
            });
        }
        catch (err) {
            var msg = "GoogleAnalyticsService.languageSearchEvent:" + errorToJsonString(err);
            this.error(msg);
            this.debug(msg);
            return;
        }
        if (!this.logToGoogle) {
            this.debug("[GoogleAnalytics] send:event:language:search:" + data);
            return;
        }
        this.ga('send', {
            hitType: 'event',
            eventCategory: 'language',
            eventAction: 'search',
            eventLabel: data
        });
    };
    GoogleAnalyticsService.prototype.projectSearchEvent = function (eventData) {
        var data;
        try {
            data = JSON.stringify({
                continent: eventData.continent,
                country: eventData.country,
                language: eventData.language,
                project: eventData.project,
                population: eventData.population,
                remainingNeed: eventData.remainNeed
            });
        }
        catch (err) {
            var msg = "GoogleAnalyticsService.projectSearchEvent:" + errorToJsonString(err);
            this.error(msg);
            this.debug(msg);
            return;
        }
        if (!this.logToGoogle) {
            this.debug("[GoogleAnalytics] send:event:project:search:" + data);
            return;
        }
        this.ga('send', {
            hitType: 'event',
            eventCategory: 'project',
            eventAction: 'search',
            eventLabel: data
        });
    };
    GoogleAnalyticsService.prototype.debug = function (msg) {
        var parts = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            parts[_i - 1] = arguments[_i];
        }
        if ((environment.debug || ({})).level !== 'debug') {
            return;
        }
        console.log.apply(console, __spread(["[debug] " + msg], parts));
    };
    return GoogleAnalyticsService;
}());
GoogleAnalyticsService.decorators = [
    { type: Injectable },
];
GoogleAnalyticsService.ctorParameters = function () { return [
    { type: BrowserService, },
]; };
var endpointRegex = new RegExp("^[^#]*?://.*?(/.*)$");
var LogLevel = {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
};
LogLevel[LogLevel.debug] = "debug";
LogLevel[LogLevel.info] = "info";
LogLevel[LogLevel.warn] = "warn";
LogLevel[LogLevel.error] = "error";
var LogService = /** @class */ (function () {
    function LogService(ga) {
        this.ga = ga;
        this.logLevel = LogLevel.info;
        if (!this.ga) {
            this.ga = ({
                error: function () {
                }
            });
        }
        var logLevel = (environment.debug || ({})).level;
        this.logLevel = (LogLevel[logLevel || 'warn']);
        if (this.logLevel === undefined) {
            var values_1 = [];
            Object.keys(LogLevel).forEach(function (key) { return values_1.push(key); });
            console.log("[error] logging system set to invalid log level (" + logLevel + "). Valid values are: " +
                (values_1.slice(values_1.length / 2).join(', ') + ". Setting to warn."));
            this.logLevel = LogLevel.warn;
        }
    }
    LogService.prototype.debug = function (msg) {
        var parts = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            parts[_i - 1] = arguments[_i];
        }
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        console.log.apply(console, __spread(["[debug] " + msg], parts));
    };
    LogService.prototype.debugJson = function (obj, msg) {
        var parts = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            parts[_i - 2] = arguments[_i];
        }
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        try {
            var json = JSON.stringify(obj, null, 2);
            var disp = (msg) ? msg + ":\n" + json : json;
            console.log.apply(console, __spread(["[debug] " + disp], parts));
        }
        catch (err) {
            console.log.apply(console, __spread(["[debug] %o\n" + msg + ":\n.debugJson encountered error: " + err, obj], parts));
        }
    };
    LogService.prototype.debugApiCall = function (path, source, options) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        this.logApiCall(path, source, options, 'call');
    };
    LogService.prototype.debugApiResponse = function (path, source, resp) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        var body;
        if (resp) {
            try {
                body = resp.json();
            }
            catch (err) {
                body = null;
            }
        }
        this.logApiCall(path, source, { body: body }, 'response');
    };
    LogService.prototype.info = function (msg) {
        var parts = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            parts[_i - 1] = arguments[_i];
        }
        if (this.logLevel > LogLevel.info) {
            return;
        }
        console.log.apply(console, __spread(["[info] " + msg], msg));
    };
    LogService.prototype.warn = function (msg) {
        var parts = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            parts[_i - 1] = arguments[_i];
        }
        if (this.logLevel > LogLevel.warn) {
            return;
        }
        console.log.apply(console, __spread(["[warn] " + msg], msg));
    };
    LogService.prototype.error = function (err, msg) {
        var parts = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            parts[_i - 2] = arguments[_i];
        }
        var message;
        if (err instanceof Response) {
            if (msg) {
                message = "[error (Response)] " + msg + ": code: " + err.status + ", " + err.statusText + ", url: " + err.url;
            }
            else {
                message = "[error (Response)] code: " + err.status + ", " + err.statusText + ", url: " + err.url;
            }
        }
        else {
            if (msg) {
                message = "[error] " + msg + ": " + (err.message || 'no error message') + "\n" + (err.stack || 'no call stack');
            }
            else {
                message = "[error] " + (err.message || 'no error message') + " " + (err.stack || 'no call stack');
            }
        }
        console.log.apply(console, __spread([message], parts));
        this.ga.error(message);
    };
    LogService.prototype.logApiCall = function (path, source, options, type) {
        var apiCalls = (environment.debug || ({})).apiCalls;
        var noBody = (environment.debug || ({})).noBody;
        if (!apiCalls) {
            return;
        }
        var matches = (typeof path === 'string') ? endpointRegex.exec(path) : '';
        var rawEndpoint = ((matches || []).length > 1) ? matches[1] : '';
        var endPoint = rawEndpoint.split('?')[0];
        if (apiCalls !== '*' && !(Array.isArray(apiCalls) && apiCalls.indexOf(endPoint) > -1)) {
            return;
        }
        source = (source || ({})).name
            || ((source || ({})).constructor || ({})).name
            || source;
        var method;
        if (typeof options.method === 'string') {
            method = options.method.toUpperCase();
        }
        else {
            switch (options.method) {
                case RequestMethod.Get:
                    method = 'GET';
                    break;
                case RequestMethod.Post:
                    method = 'POST';
                    break;
                case RequestMethod.Put:
                    method = 'PUT';
                    break;
                case RequestMethod.Delete:
                    method = 'DELETE';
                    break;
                case RequestMethod.Options:
                    method = 'OPTIONS';
                    break;
                case RequestMethod.Head:
                    method = 'HEAD';
                    break;
                case RequestMethod.Patch:
                    method = 'PATCH';
                    break;
                default:
                    method = (type === 'call')
                        ? 'UNKNOWN_METHOD-' + options.method
                        : null;
                    break;
            }
        }
        var json = '{suppressed by debug config}';
        if (!noBody || (noBody !== '*' && (Array.isArray(noBody) && noBody.indexOf(endPoint) === -1))) {
            try {
                json = options.body ? JSON.stringify(options.body, null, 2) : options.body;
            }
            catch (err) {
                json = "non-json-compliant-response: " + options.body;
            }
        }
        console.log("[debug-api-" + type + "] source: " + source + ", " + (method || '') + (method ? ':' : '') + path + ", body: " + json);
    };
    return LogService;
}());
LogService.decorators = [
    { type: Injectable },
];
LogService.ctorParameters = function () { return [
    { type: GoogleAnalyticsService, },
]; };
var ObjectId = /** @class */ (function () {
    function ObjectId(id) {
        this.id = '';
        this.id = id || '';
    }
    Object.defineProperty(ObjectId.prototype, "timeStamp", {
        get: function () {
            return new Date(parseInt(this.id.substring(0, 8), 16) * 1000);
        },
        enumerable: true,
        configurable: true
    });
    ObjectId.prototype.toString = function () {
        return this.id;
    };
    return ObjectId;
}());
var ServerDate = /** @class */ (function () {
    function ServerDate() {
    }
    ServerDate.fromEpochSeconds = function (seconds) {
        var d = new Date(0);
        d.setUTCSeconds(seconds);
        return d;
    };
    return ServerDate;
}());
var Address = /** @class */ (function () {
    function Address() {
    }
    Address.fromJson = function (json) {
        json = json || {};
        var obj = new Address();
        obj.address1 = json.address1 || json.addressLine1 || '';
        obj.address2 = json.address2 || json.addressLine2 || '';
        obj.city = json.city || json.addressCity || '';
        obj.country = json.country || '';
        obj.state = json.state || json.addressState || '';
        obj.zip = json.zip || json.addressZip || '';
        return obj;
    };
    Address.fromJsonArray = function (jsons) {
        var results = [];
        if (!Array.isArray(jsons)) {
            try {
                for (var _a = __values([jsons]), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var json = _b.value;
                    results.push(Address.fromJson(json));
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return results;
        var e_1, _c;
    };
    return Address;
}());
var BillingDetails = /** @class */ (function () {
    function BillingDetails() {
    }
    BillingDetails.fromJson = function (json) {
        json = json || {};
        var obj = new BillingDetails();
        obj.brand = json.brand || '';
        obj.expiresMonth = json.expiresMonth || '';
        obj.expiresYear = json.expiresYear || '';
        obj.last4 = json.last4 || '';
        obj.name = json.name || '';
        return obj;
    };
    return BillingDetails;
}());
var User = /** @class */ (function () {
    function User() {
    }
    Object.defineProperty(User.prototype, "fullName", {
        get: function () {
            return "" + (this.firstName || '') + (this.lastName && this.lastName !== '' ? ' ' : '') + (this.lastName || '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "created", {
        get: function () {
            return (this.id || ({})).timeStamp || null;
        },
        enumerable: true,
        configurable: true
    });
    User.fromJson = function (json) {
        json = json || {};
        var obj = new User();
        obj.id = json.id;
        obj.firstName = json.firstName;
        obj.lastName = json.lastName;
        obj.email = json.email;
        obj.phone = json.phone;
        obj.mailingAddress = Address.fromJson(json.mailingAddress);
        obj.billingAddress = Address.fromJson(json.billingAddress);
        obj.billingDetails = BillingDetails.fromJson(json.billingDetails);
        return obj;
    };
    User.fromJsonArray = function (jsons) {
        var results = [];
        if (!Array.isArray(jsons)) {
            try {
                for (var _a = __values([jsons]), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var json = _b.value;
                    results.push(User.fromJson(json));
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        return results;
        var e_2, _c;
    };
    return User;
}());
var moment = momentImported;
var AuthenticationToken = /** @class */ (function () {
    function AuthenticationToken(userId, email, phone, domain, firstName, lastName, issued, expires, audience, issuer, jwtId, jwtToken, key) {
        this.userId = userId;
        this.email = email;
        this.phone = phone;
        this.domain = domain;
        this.firstName = firstName;
        this.lastName = lastName;
        this.issued = issued;
        this.expires = expires;
        this.audience = audience;
        this.issuer = issuer;
        this.jwtId = jwtId;
        this.jwtToken = jwtToken;
        this.key = key;
    }
    Object.defineProperty(AuthenticationToken.prototype, "userCreated", {
        get: function () {
            return (this.userId || ({})).timeStamp;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthenticationToken.prototype, "expired", {
        get: function () {
            return moment().isAfter(this.expires);
        },
        enumerable: true,
        configurable: true
    });
    AuthenticationToken.fromJson = function (json) {
        json = json || {};
        return new AuthenticationToken(new ObjectId(json.id || (json.userId || ({})).id), json.email, json.phone, json.domain, json.firstName, json.lastName, ServerDate.fromEpochSeconds(json.iat), ServerDate.fromEpochSeconds(json.exp), json.aud, json.iss, json.jti, json.token || json.jwtToken, json.key);
    };
    AuthenticationToken.fromJsonArray = function (jsons) {
        var results = [];
        if (Array.isArray(jsons)) {
            try {
                for (var jsons_1 = __values(jsons), jsons_1_1 = jsons_1.next(); !jsons_1_1.done; jsons_1_1 = jsons_1.next()) {
                    var json = jsons_1_1.value;
                    results.push(AuthenticationToken.fromJson(json));
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (jsons_1_1 && !jsons_1_1.done && (_a = jsons_1.return)) _a.call(jsons_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
        return results;
        var e_3, _a;
    };
    AuthenticationToken.fromJwt = function (key, jwt) {
        jwt = jwt || '';
        var jwtPayload;
        try {
            var jsonParts = jwt.split('.');
            if (jsonParts.length !== 3) {
                throw new Error("invalid JWT token, should have 3 parts, but had " + jsonParts.length);
            }
            var decoded = decode(jsonParts[1]).toString();
            jwtPayload = JSON.parse(decoded);
        }
        catch (err) {
            new LogService().error(err, 'problem with decoding AuthenticationToken from json');
            new LogService().debugJson(jwt, 'errant auth token');
        }
        jwtPayload.token = jwt;
        jwtPayload.key = key;
        return AuthenticationToken.fromJson(jwtPayload);
    };
    AuthenticationToken.fromTokenMap = function (tokenMap) {
        var tokens = (tokenMap || { token: {} }).token;
        var results = [];
        try {
            for (var _a = __values(Object.keys(tokens)), _b = _a.next(); !_b.done; _b = _a.next()) {
                var key = _b.value;
                results.push(AuthenticationToken.fromJwt(key, tokens[key]));
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_4) throw e_4.error; }
        }
        return results;
        var e_4, _c;
    };
    AuthenticationToken.prototype.toUser = function () {
        return User.fromJson({
            id: this.userId,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            phone: this.phone
        });
    };
    return AuthenticationToken;
}());
var PREFIX = 'app';
var StorageService = /** @class */ (function () {
    function StorageService(browserService) {
        var _this = this;
        this.browserService = browserService;
        this.subjects = {
            local: {},
            session: {},
            property: function (type) {
                return _this.subjects[_this.property(type)];
            }
        };
        this.types = ['local', 'session'];
        this.store = {
            local: null,
            session: null
        };
        this.local = {
            clear: function () {
                _this.clear(0);
            },
            getItem: function (key) {
                return _this.getItem(key, 0);
            },
            getJson: function (key) {
                return _this.getJson(key, 0);
            },
            getObserver: function (key) {
                return _this.getObserver(key, 0);
            },
            key: function (n) {
                return _this.key(n, 0);
            },
            length: function () {
                return _this.length(0);
            },
            removeItem: function (key) {
                _this.removeItem(key, 0);
            },
            removeParentItem: function (key) {
                _this.removeParentItem(key, 0);
            },
            setItem: function (key, value) {
                _this.setItem(key, value, 0);
            },
            setJson: function (key, value) {
                _this.setJson(key, value, 0);
            },
            setParentJson: function (key, value) {
                _this.setParentJson(key, value, 0);
            }
        };
        this.session = {
            clear: function () {
                _this.clear(1);
            },
            getItem: function (key) {
                return _this.getItem(key, 1);
            },
            getJson: function (key) {
                return _this.getJson(key, 1);
            },
            getObserver: function (key) {
                return _this.getObserver(key, 1);
            },
            key: function (n) {
                return _this.key(n, 1);
            },
            length: function () {
                return _this.length(1);
            },
            removeItem: function (key) {
                _this.removeItem(key, 1);
            },
            removeParentItem: function (key) {
                _this.removeParentItem(key, 0);
            },
            setItem: function (key, value) {
                _this.setItem(key, value, 1);
            },
            setJson: function (key, value) {
                _this.setJson(key, value, 1);
            },
            setParentJson: function (key, value) {
                _this.setParentJson(key, value, 1);
            }
        };
        this.store.local = browserService.localStorage;
        this.store.session = browserService.sessionStorage;
    }
    StorageService.prototype.buildKey = function (key) {
        return PREFIX + "-" + key;
    };
    StorageService.prototype.clear = function (type) {
        this.engine(type).clear();
        var subjects = this.subjects.property(type);
        for (var key in subjects) {
            if (subjects.hasOwnProperty(key)) {
                subjects[key].complete();
            }
        }
        this.subjects[this.property(type)] = {};
    };
    StorageService.prototype.engine = function (type) {
        return this.store[this.property(type)];
    };
    StorageService.prototype.getItem = function (key, type) {
        return this.engine(type).getItem(this.buildKey(key));
    };
    StorageService.prototype.getJson = function (key, type) {
        return JSON.parse(this.engine(type).getItem(this.buildKey(key)));
    };
    StorageService.prototype.getObserver = function (key, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        return this.subjects.property(type)[key].asObservable();
    };
    StorageService.prototype.key = function (n, type) {
        return this.engine(type).key(n);
    };
    StorageService.prototype.length = function (type) {
        return this.engine(type).length;
    };
    StorageService.prototype.property = function (type) {
        return this.types[type];
    };
    StorageService.prototype.removeItem = function (key, type) {
        this.engine(type).removeItem(this.buildKey(key));
        if (this.subjects.property(type)[key]) {
            this.subjects.property(type)[key].complete();
        }
        delete this.subjects[this.property(type)][key];
    };
    StorageService.prototype.removeParentItem = function (key, type) {
        if (type === 1) {
            this.browserService.window.opener.sessionStorage.removeItem(this.buildKey(key));
        }
        else {
            this.browserService.window.opener.localStorage.removeItem(this.buildKey(key));
        }
        if (this.subjects.property(type)[key]) {
            this.subjects.property(type)[key].complete();
        }
        delete this.subjects[this.property(type)][key];
    };
    StorageService.prototype.setItem = function (key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        this.engine(type).setItem(this.buildKey(key), value);
        this.subjects.property(type)[key].next(value);
    };
    StorageService.prototype.setJson = function (key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        this.engine(type).setItem(this.buildKey(key), JSON.stringify(value));
        this.subjects.property(type)[key].next(value);
    };
    StorageService.prototype.setParentJson = function (key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        if (type === 1) {
            this.browserService.window.opener.sessionStorage.setItem(this.buildKey(key), JSON.stringify(value));
        }
        else {
            this.browserService.window.opener.localStorage.setItem(this.buildKey(key), JSON.stringify(value));
        }
        this.subjects.property(type)[key].next(value);
    };
    return StorageService;
}());
StorageService.decorators = [
    { type: Injectable },
];
StorageService.ctorParameters = function () { return [
    { type: BrowserService, },
]; };
var AUTH_STORAGE_KEY = 'ilb_auth';
var AuthenticationStorageService = /** @class */ (function () {
    function AuthenticationStorageService(store, log) {
        this.store = store;
        this.log = log;
    }
    AuthenticationStorageService.prototype.getAuthenticationToken = function (service) {
        var tokensJson = (this.store.local.getItem(AUTH_STORAGE_KEY) || this.store.session.getItem(AUTH_STORAGE_KEY));
        if (!tokensJson) {
            return null;
        }
        var tokens;
        try {
            tokens = JSON.parse(tokensJson);
            if (!Array.isArray(tokens)) {
                this.log.error(new Error('stored tokens should have been in an array'));
            }
        }
        catch (err) {
            this.log.info(err, 'stored auth tokens are corrupted... deleting them from the store');
            this.clearTokens();
            return null;
        }
        try {
            for (var tokens_1 = __values(tokens), tokens_1_1 = tokens_1.next(); !tokens_1_1.done; tokens_1_1 = tokens_1.next()) {
                var token = tokens_1_1.value;
                if (token.key === service) {
                    return AuthenticationToken.fromJson(token);
                }
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (tokens_1_1 && !tokens_1_1.done && (_a = tokens_1.return)) _a.call(tokens_1);
            }
            finally { if (e_5) throw e_5.error; }
        }
        return null;
        var e_5, _a;
    };
    AuthenticationStorageService.prototype.getAuthenticationTokens = function () {
        var tokensJson = (this.store.local.getItem(AUTH_STORAGE_KEY) || this.store.session.getItem(AUTH_STORAGE_KEY));
        if (!tokensJson) {
            return null;
        }
        var tokens;
        try {
            tokens = JSON.parse(tokensJson);
            if (!Array.isArray(tokens)) {
                this.log.error(new Error('stored tokens should have been in an array'));
            }
        }
        catch (err) {
            this.log.info(err, 'stored auth tokens are corrupted... deleting them from the store');
            this.clearTokens();
            return null;
        }
        return AuthenticationToken.fromJsonArray(tokens);
    };
    AuthenticationStorageService.prototype.saveTokens = function (tokens, remember) {
        this.clearTokens();
        var store = (remember) ? this.store.local : this.store.session;
        store.setItem(AUTH_STORAGE_KEY, JSON.stringify(tokens));
    };
    AuthenticationStorageService.prototype.clearTokens = function () {
        this.store.session.removeItem(AUTH_STORAGE_KEY);
        this.store.local.removeItem(AUTH_STORAGE_KEY);
    };
    return AuthenticationStorageService;
}());
AuthenticationStorageService.decorators = [
    { type: Injectable },
];
AuthenticationStorageService.ctorParameters = function () { return [
    { type: StorageService, },
    { type: LogService, },
]; };
var ApiService = /** @class */ (function () {
    function ApiService(service, authStorage, http, log) {
        this.service = service;
        this.authStorage = authStorage;
        this.http = http;
        this.log = log;
        this.source = { name: 'source not set' };
        this._authError$ = new Subject();
        this._serverDown$ = new Subject();
        this.baseUrl = "" + environment.services[service];
        if (!(environment.services || ({}))[service]) {
            log.error(new Error(this.constructor.name + " needs configuration 'environment.services." + service + "' to be defined "
                + "in order to properly function."));
        }
    }
    Object.defineProperty(ApiService.prototype, "authError$", {
        get: function () {
            return this._authError$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiService.prototype, "serverDown$", {
        get: function () {
            return this._serverDown$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    ApiService.prototype.request = function (endpoint, options, apiOptions) {
        var _this = this;
        options = (options || { method: 'GET' });
        apiOptions = apiOptions || {};
        var url = "" + this.baseUrl + endpoint;
        this.log.debugApiCall(url, this.source, options);
        var authToken = this.authStorage.getAuthenticationToken(this.service);
        var headers = Object.assign({}, options.headers);
        headers['Authorization'] = headers['Authorization']
            || ((authToken) ? "Bearer " + (authToken || (({}))).jwtToken : undefined);
        headers['Content-Type'] = headers['Content-Type'] || 'application/json';
        try {
            for (var _a = __values(Object.keys(headers)), _b = _a.next(); !_b.done; _b = _a.next()) {
                var key = _b.value;
                if (headers[key] === undefined || headers[key] === null) {
                    delete headers[key];
                }
            }
        }
        catch (e_6_1) { e_6 = { error: e_6_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_6) throw e_6.error; }
        }
        options.headers = (new HttpHeaders(headers));
        return this
            .http
            .request(options.method, url, options)
            .do(function (req) {
            _this.log.debugApiResponse(url, _this.source, req);
        })
            .catch(function (res) {
            switch (res.status) {
                case 0:
                    _this.log.error(res, ">>server down<<: " + res + " - if the server is up, make sure this isn't a CORS error");
                    _this._serverDown$.next();
                    return Observable.throw(res);
                case 401:
                    _this.log.debug("authentication error: %o", res);
                    if (!apiOptions.disableServerDown) {
                        _this._authError$.next(authToken);
                    }
                    else {
                        _this.log.debug('authError$ not emitted since apiOptions.disableServerDown === true');
                    }
                    return Observable.throw(res);
                case 403:
                    return Observable.throw(res);
                default:
                    _this.log.error(res, 'Unexpected Error');
                    return Observable.throw(res);
            }
        });
        var e_6, _c;
    };
    ApiService.prototype.get = function (url, options, apiOptions) {
        options = options || {};
        options.method = 'GET';
        return this.request(url, options, apiOptions);
    };
    ApiService.prototype.post = function (url, body, options, apiOptions) {
        options = options || {};
        options.method = 'POST';
        options.body = body;
        return this.request(url, options, apiOptions);
    };
    ApiService.prototype.put = function (url, body, options, apiOptions) {
        options = options || {};
        options.method = 'PUT';
        options.body = body;
        return this.request(url, options, apiOptions);
    };
    ApiService.prototype.delete = function (url, options, apiOptions) {
        options = options || {};
        options.method = 'DELETE';
        return this.request(url, options, apiOptions);
    };
    ApiService.prototype.patch = function (url, body, options, apiOptions) {
        options = options || {};
        options.method = 'PATCH';
        options.body = body;
        return this.request(url, options, apiOptions);
    };
    ApiService.prototype.head = function (url, options, apiOptions) {
        options = options || {};
        options.method = 'HEAD';
        return this.request(url, options, apiOptions);
    };
    return ApiService;
}());
var SERVICE_AUDIENCE = 'score.seedconnect.com';
var SisenseApiService = /** @class */ (function (_super) {
    __extends(SisenseApiService, _super);
    function SisenseApiService(authStorage, http, log) {
        var _this = this;
        if (!environment.services || !environment.services[SERVICE_AUDIENCE]) {
            throw new Error("environment.services is misconfigured for SisenseApiService, expecting key " + SERVICE_AUDIENCE);
        }
        _this = _super.call(this, SERVICE_AUDIENCE, authStorage, http, log) || this;
        return _this;
    }
    return SisenseApiService;
}(ApiService));
SisenseApiService.decorators = [
    { type: Injectable },
];
SisenseApiService.ctorParameters = function () { return [
    { type: AuthenticationStorageService, },
    { type: HttpClient, },
    { type: LogService, },
]; };
var Language = /** @class */ (function () {
    function Language() {
    }
    Language.fromJson = function (json) {
        var lang = new Language();
        lang.continent = json.Continent || json.continent || '';
        lang.country = json.Country || json.country || '';
        lang.latitude = json.Latitude || json.latitude || 0;
        lang.longitude = json.Longitude || json.longitude || 0;
        lang.languageName = json.LanguageName || json.languageName || '';
        lang.languageId = json.LanguageID || json.languageId || '';
        lang.campaignId = json.CampaignID || json.campaignId || '';
        lang.population = Number.isInteger(json.Population) ? json.Population : Number.parseInt(json.Population);
        lang.populationRange = json.PopulationRange || json.populationRange || '';
        lang.beginYear = json.BeginYr || json.beginYr || 0;
        lang.projectCount = json.ProjectCount || json.projectCount || 0;
        lang.orgCount = json.OrgCount || json.orgCount || 0;
        lang.featuredLanguageFlag = (json.FeaturedLanguageFlag === 'Y') || json.featuredLanguageFlag || false;
        lang.isConfidentialLanguageFlag = (json.IsConfidentialLanguageFlag === 'Y') || json.isConfidentialLanguageFlag || false;
        lang.isSignLanguageFlag = (json.IsSignLanguageFlag === 'Y') || json.isSignLanguageFlag || false;
        lang.projectSensitivityLevel = json.ProjectSensitivityLevel || json.projectSensitivityLevel || 0;
        lang.primaryReligion = json.PrimaryReligion || json.primaryReligion || '';
        lang.prayerCommitCount = json.PrayerCommitCnt || json.prayerCommitCount || 0;
        lang.commonFrameworkFlag = (json.CommonFrameworkFlag === 'Y') || json.commonFrameworkFlag || false;
        lang.firstScriptureFlag = (json.FirstScriptureFlag === 'Y') || json.firstScriptureFlag || false;
        lang.joshuaProjectLeastReachedFlag = (json.JoshuaProjectLeastReachedFlag === 'Y')
            || json.commonFrameworkFlag || false;
        lang.completedFullBibleFlag = (json.CompletedFullBibleFlag === 'Y') || json.completedFullBibleFlag || false;
        lang.completedNewTestamentFlag = (json.CompletedNewTestamentFlag === 'Y') || json.completedNewTestamentFlag || false;
        lang.completedJesusFilmFlag = (json.CompletedJesusFilmFlag === 'Y') || json.completedJesusFilmFlag || false;
        lang.completedAudioRecordingsFlag = (json.CompletedAudioRecordingsFlag === 'Y')
            || json.completedAudioRecordingsFlag || false;
        lang.completed25ChaptersFlag = (json.Completed25ChaptersFlag === 'Y') || json.completed25ChaptersFlag || false;
        lang.translationInProgressFlag = (json.TranslationInProgressFlag === 'Y') || json.translationInProgressFlag || false;
        lang.noTranslationYetFlag = (json.NoTranslationYetFlag === 'Y') || json.noTranslationYetFlag || false;
        lang.translationTypeWrittenTranslationFlag = (json.TranslationTypeWrittenTranslationFlag === 'Y')
            || json.translationTypeWrittenTranslationFlag || false;
        lang.translationTypeWrittenStoriesFlag = (json.TranslationTypeWrittenStoriesFlag === 'Y')
            || json.translationTypeWrittenStoriesFlag || false;
        lang.translationTypeOralTranslationFlag = (json.TranslationTypeOralTranslationFlag === 'Y')
            || json.translationTypeOralTranslationFlag || false;
        lang.translationTypeOralStoryingFlag = (json.TranslationTypeOralStoryingFlag === 'Y')
            || json.translationTypeOralStoryingFlag || false;
        lang.translationTypeSignLanguageFlag = (json.TranslationTypeSignLanguageFlag === 'Y')
            || json.translationTypeSignLanguageFlag || false;
        lang.translationTypeFilmFlag = (json.TranslationTypeFilmFlag === 'Y') || json.translationTypeFilmFlag || false;
        lang.translationTypeOtherFlag = (json.TranslationTypeOtherFlag === 'Y') || json.translationTypeOtherFlag || false;
        lang.goalsFullBibleFlag = (json.GoalsFullBibleFlag === 'Y') || json.goalsFullBibleFlag || false;
        lang.goalsFullNTFlag = (json.GoalsFullNTFlag === 'Y') || json.goalsFullNTFlag || false;
        lang.goalsFullOTFlag = (json.GoalsFullOTFlag === 'Y') || json.goalsFullOTFlag || false;
        lang.goalsAGospelFlag = (json.GoalsAGospelFlag === 'Y') || json.goalsAGospelFlag || false;
        lang.goalsGenesisFlag = (json.GoalsGenesisFlag === 'Y') || json.goalsGenesisFlag || false;
        lang.goalsNtPortionsFlag = (json.GoalsNTPortionsFlag === 'Y') || json.goalsNtPortionsFlag || false;
        lang.goalsOtPortionsFlag = (json.GoalsOTPortionsFlag === 'Y') || json.goalsOtPortionsFlag || false;
        lang.goalsJesusFilmFlag = (json.GoalsJesusFilmFlag === 'Y') || json.goalsJesusFilmFlag || false;
        lang.goalsBibleStoriesFlag = (json.GoalsBibleStoriesFlag === 'Y') || json.goalsBibleStoriesFlag || false;
        lang.langLifetimeFundingGoal = json.LangLifetimeFundingGoal || json.langLifetimeFundingGoal || 0;
        lang.langLifetimeRemainNeed = Math.max(0, json.LangLifetimeRemainNeed || 0, json.langLifetimeRemainNeed || 0);
        lang.beforeIBibleDonationsLangLifetimeRemainNeed = Math.max(0, json.BeforeIBibleDonationsLangLifetimeRemainNeed || 0, json.beforeIBibleDonationsLangLifetimeRemainNeed || 0);
        lang.totalLanguagePledges = json.TotalLanguagePledges || json.langLifetimeFundingGoal || 0;
        lang.totalLanguageGiving = json.TotalLanguageGiving || json.langLifetimeFundingGoal || 0;
        lang.beforeIBibleTotalLanguageGiving = json.BeforeIBibleTotalLanguageGiving || json.langLifetimeFundingGoal || 0;
        lang.iBibleOnlyTotalRemainingNeed = Math.max(0, json.IBibleOnlyTotalRemainingNeed || 0, json.iBibleOnlyTotalRemainingNeed || 0);
        lang.imageSrc = json.ImageSrc || json.imageSrc || '';
        lang.lowResImageSrc = json.LowResImageSrc || json.lowResImageSrc || '';
        lang.description = json.Description || json.description || '';
        lang.gathering2017Flag = (json.Gathering2017Flag === 'Y') || json.gathering2017Flag || false;
        lang.visibleFlag = (json.VisibleFlag === 'Y') || json.visibleFlag || false;
        lang.region = json.Region || json.region || '';
        lang.totalDonors = json.TotalDonors || json.totalDonors || 0;
        return lang;
    };
    Language.fromSisenseEntry = function (headers, values) {
        var json = {};
        var i = 0;
        try {
            for (var headers_1 = __values(headers), headers_1_1 = headers_1.next(); !headers_1_1.done; headers_1_1 = headers_1.next()) {
                var key = headers_1_1.value;
                json[key] = values[i];
                i++;
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (headers_1_1 && !headers_1_1.done && (_a = headers_1.return)) _a.call(headers_1);
            }
            finally { if (e_7) throw e_7.error; }
        }
        return Language.fromJson(json);
        var e_7, _a;
    };
    Language.fromSisenseResponseArray = function (json) {
        json = json || {};
        var headers = json.headers || [];
        var values = json.values || [];
        var languages = [];
        try {
            for (var values_2 = __values(values), values_2_1 = values_2.next(); !values_2_1.done; values_2_1 = values_2.next()) {
                var value = values_2_1.value;
                languages.push(Language.fromSisenseEntry(headers, value));
            }
        }
        catch (e_8_1) { e_8 = { error: e_8_1 }; }
        finally {
            try {
                if (values_2_1 && !values_2_1.done && (_a = values_2.return)) _a.call(values_2);
            }
            finally { if (e_8) throw e_8.error; }
        }
        return languages;
        var e_8, _a;
    };
    Object.defineProperty(Language.prototype, "bannerImage", {
        get: function () {
            return this.buildImageFilename + '-1440x800.png';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Language.prototype, "cardImage", {
        get: function () {
            return this.buildImageFilename + '-404x210.png';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Language.prototype, "buildImageFilename", {
        get: function () {
            var filename = environment.baseImageUrl || 'https://images.illuminations.bible/';
            if (this.region === 'Northern America') {
                filename += 'North-America';
            }
            else {
                filename += this.continent;
            }
            return filename;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Language.prototype, "cartId", {
        get: function () {
            return this.languageId.toLowerCase();
        },
        enumerable: true,
        configurable: true
    });
    Language.prototype.copy = function () {
        var newLang = new Language();
        var keys = Object.getOwnPropertyNames(this);
        try {
            for (var keys_1 = __values(keys), keys_1_1 = keys_1.next(); !keys_1_1.done; keys_1_1 = keys_1.next()) {
                var key = keys_1_1.value;
                newLang[key] = this[key];
            }
        }
        catch (e_9_1) { e_9 = { error: e_9_1 }; }
        finally {
            try {
                if (keys_1_1 && !keys_1_1.done && (_a = keys_1.return)) _a.call(keys_1);
            }
            finally { if (e_9) throw e_9.error; }
        }
        return newLang;
        var e_9, _a;
    };
    Language.prototype.toJson = function () {
        return JSON.stringify(this, Object.getOwnPropertyNames(this));
    };
    return Language;
}());
var cacheOptions = {
    cacheLife: 1000 * 60 * 5
};
var LanguageService = /** @class */ (function () {
    function LanguageService(api) {
        this.api = api;
        this.projectSearchFlag = false;
    }
    LanguageService.prototype.getContinents = function () {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .pluck('continent')
            .distinct(function (c) { return c; })
            .toArray();
    };
    LanguageService.prototype.getCountries = function () {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .pluck('country')
            .distinct(function (c) { return c; })
            .toArray();
    };
    LanguageService.prototype.getCountriesByContinent = function (continent) {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .filter(function (language) { return language.continent.toLowerCase() === continent.toLowerCase(); })
            .pluck('country')
            .distinct(function (c) { return c; })
            .toArray();
    };
    LanguageService.prototype.getFeaturedLanguages = function () {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .filter(function (language) { return language.featuredLanguageFlag; })
            .toArray();
    };
    LanguageService.prototype.getFilteredLanguages = function () {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .filter(function (language) { return (language.gathering2017Flag); })
            .toArray();
    };
    LanguageService.prototype.getLanguageById = function (languageId) {
        return this
            .getLanguages()
            .concatMap(function (languages) {
            return Observable
                .from(languages)
                .first(function (language) { return language.languageId === languageId; }, null, null);
        });
    };
    LanguageService.prototype.getLanguageNames = function () {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .pluck('languageName')
            .distinct(function (ln) { return ln; })
            .toArray();
    };
    LanguageService.prototype.getLanguages = function () {
        var _this = this;
        if (!this.cache$) {
            this
                .cache$ = Observable
                .defer(function () {
                return _this
                    .api
                    .get('/sql?query=select%20*%20from%20Languages')
                    .concatMap(function (json) {
                    var languages = Language.fromSisenseResponseArray(json);
                    var results = [];
                    try {
                        for (var languages_1 = __values(languages), languages_1_1 = languages_1.next(); !languages_1_1.done; languages_1_1 = languages_1.next()) {
                            var language = languages_1_1.value;
                            if (language.visibleFlag) {
                                results.push(language);
                            }
                        }
                    }
                    catch (e_10_1) { e_10 = { error: e_10_1 }; }
                    finally {
                        try {
                            if (languages_1_1 && !languages_1_1.done && (_a = languages_1.return)) _a.call(languages_1);
                        }
                        finally { if (e_10) throw e_10.error; }
                    }
                    return results;
                    var e_10, _a;
                })
                    .toArray();
            })
                .publishReplay(1, cacheOptions.cacheLife)
                .refCount()
                .take(1);
        }
        return (this.cache$);
    };
    LanguageService.prototype.getLanguagesByContinent = function (continent) {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .filter(function (language) { return language.continent.toLowerCase() === continent.toLowerCase(); })
            .pluck('languageName')
            .distinct(function (ln) { return ln; })
            .toArray();
    };
    LanguageService.prototype.getLanguagesByCountry = function (country) {
        return this
            .getLanguages()
            .concatMap(function (languagesArray) { return Observable.from(languagesArray); })
            .filter(function (language) { return language.country.toLocaleLowerCase() === country.toLocaleLowerCase(); })
            .pluck('languageName')
            .distinct(function (ln) { return ln; })
            .toArray();
    };
    LanguageService.prototype.getTranslationType = function (language) {
        var translationType = '';
        if (language.translationTypeWrittenTranslationFlag
            || language.translationTypeWrittenStoriesFlag) {
            translationType += 'Written,';
        }
        if (language.translationTypeOralTranslationFlag
            || language.translationTypeOralStoryingFlag) {
            translationType += ' Oral,';
        }
        if (language.translationTypeFilmFlag) {
            translationType += ' Film,';
        }
        if (language.translationTypeOtherFlag) {
            translationType += ' Other,';
        }
        if (language.translationTypeSignLanguageFlag) {
            translationType = ' Sign Language Video,';
        }
        if (translationType === '') {
            translationType = 'Oral,';
        }
        translationType = translationType.slice(0, -1);
        return translationType;
    };
    return LanguageService;
}());
LanguageService.decorators = [
    { type: Injectable },
];
LanguageService.ctorParameters = function () { return [
    { type: SisenseApiService, },
]; };
var Project = /** @class */ (function () {
    function Project() {
    }
    Project.fromJson = function (json) {
        var project = new Project();
        project.projectId = json.ProjectID || json.projectId || '';
        project.clusterId = json.ClusterID || json.clusterId || '';
        project.campaignId = json.CampaignID || json.clusterId || '';
        project.projectName = json.ProjectName || json.projectName || '';
        project.languageId = json.LanguageID || json.languageId || '';
        project.stgStatusName = json.StgStatusName || json.stgStatusName || '';
        project.pipelineFlag = (json.PipelineFlag === 'Y') || json.pipelineFlag || false;
        project.organizationId = json.OrganizationID || json.organizationId || '';
        project.projectLifetimeFundingGoal = json.ProjectLifetimeFundingGoal
            || json.projectLifetimeFundingGoal || 0;
        project.projectLifetimeRemainNeed = Math.max(0, json.ProjectLifetimeRemainNeed
            || json.projectLifetimeRemainNeed || 0);
        project.beforeIBibleDonationsRemainingNeed = Math.max(0, json.BeforeIBibleDonationsRemainingNeed || 0, json.beforeIBibleDonationsRemainingNeed || 0);
        project.totalPledges = json.TotalPledges || json.totalPledges || 0;
        project.totalGiven = json.TotalGiven || json.totalGiven || 0;
        project.beforeIBibleTotalGiven = json.BeforeIBibleTotalGiven || json.beforeIBibleTotalGiven || 0;
        project.iBibleOnlyTotalGiven = json.IBibleOnlyTotalGiven || json.iBibleOnlyTotalGiven || 0;
        project.languageCount = json.LanguageCount || json.languageCount || 0;
        project.isClusterFlag = json.IsClusterFlag === 'Y';
        project.commonFrameworkFlag = (json.CommonFrameworkFlag === 'Y') || json.commonFrameworkFlag || false;
        project.featuredProjectFlag = (json.FeaturedProjectFlag === 'Y') || json.featuredProjectFlag || false;
        project.prayerCommitCnt = json.PrayerCommitCnt || json.prayerCommitCnt || 0;
        project.beginYr = json.BeginYr || json.beginYr || 0;
        project.sensitivityLevel = json.SensitivityLevel || json.sensitivityLevel || 0;
        project.firstScriptureFlag = (json.FirstScriptureFlag === 'Y') || json.firstScriptureFlag || false;
        project.translationTypeWrittenTranslationFlag = (json.TranslationTypeWrittenTranslationFlag === 'Y')
            || json.translationTypeWrittenTranslationFlag || false;
        project.translationTypeWrittenStoriesFlag = (json.TranslationTypeWrittenStoriesFlag === 'Y')
            || json.translationTypeWrittenStoriesFlag || false;
        project.translationTypeOralTranslationFlag = (json.TranslationTypeOralTranslationFlag === 'Y')
            || json.translationTypeOralTranslationFlag || false;
        project.translationTypeOralStoryingFlag = (json.TranslationTypeOralStoryingFlag === 'Y')
            || json.translationTypeOralStoryingFlag || false;
        project.translationTypeSignLanguageFlag = (json.TranslationTypeSignLanguageFlag === 'Y')
            || json.translationTypeSignLanguageFlag || false;
        project.translationTypeFilmFlag = (json.TranslationTypeFilmFlag === 'Y')
            || json.translationTypeFilmFlag || false;
        project.translationTypeOtherGoalsFlag = (json.TranslationTypeOtherGoalsFlag === 'Y')
            || json.translationTypeOtherFlag || false;
        project.goalsFullBibleFlag = (json.FullBibleFlag === 'Y') || json.goalsFullBibleFlag || false;
        project.goalsFullNTFlag = (json.GoalsFullNTFlag === 'Y') || json.goalsFullNTFlag || false;
        project.goalsFullOTFlag = (json.GoalsFullOTFlag === 'Y') || json.goalsFullOTFlag || false;
        project.goalsAGospelFlag = (json.GoalsAGospelFlag === 'Y') || json.goalsAGospelFlag || false;
        project.goalsGenesisFlag = (json.GoalsGenesisFlag === 'Y') || json.goalsGenesisFlag || false;
        project.goalsNtPortionsFlag = (json.GoalsNTPortionsFlag === 'Y') || json.goalsNtPortionsFlag || false;
        project.goalsOtPortionsFlag = (json.GoalsOTPortionsFlag === 'Y') || json.goalsOtPortionsFlag || false;
        project.goalsJesusFilmFlag = (json.GoalsJesusFilmFlag === 'Y') || json.goalsJesusFilmFlag || false;
        project.goalsBibleStoriesFlag = (json.GoalsBibleStoriesFlag === 'Y') || json.goalsBibleStoriesFlag || false;
        project.gathering2017Flag = (json.Gathering2017Flag === 'Y') || json.gathering2017Flag || false;
        project.visibleFlag = (json.VisibleFlag === 'Y') || json.visibleFlag || false;
        project.population = json.ProjectPopulation || json.population || 0;
        project.populationRange = json.ProjectPopulationRange || json.populationRange || '';
        project.projectDialectFlag = (json.ProjectDialectFlag === 'Y') || json.firstScriptureFlag || false;
        project.description = json.ProjectDescriptio || json.projectDescription || '';
        project.visibleFlag = (json.VisibleFlag === 'Y') || json.firstScriptureFlag || false;
        project.bannerImage = this.buildImageFilename(project) + '-1440x800.png';
        project.cardImage = this.buildImageFilename(project) + '404x210.png';
        project.meta = {};
        project.totalDonors = json.TotalDonors || json.totalDonors || 0;
        project.countryCount = json.countryCount || 1;
        return project;
    };
    Project.fromSisenseEntry = function (headers, values) {
        var json = {};
        var i = 0;
        try {
            for (var headers_2 = __values(headers), headers_2_1 = headers_2.next(); !headers_2_1.done; headers_2_1 = headers_2.next()) {
                var key = headers_2_1.value;
                json[key] = values[i];
                i++;
            }
        }
        catch (e_11_1) { e_11 = { error: e_11_1 }; }
        finally {
            try {
                if (headers_2_1 && !headers_2_1.done && (_a = headers_2.return)) _a.call(headers_2);
            }
            finally { if (e_11) throw e_11.error; }
        }
        return Project.fromJson(json);
        var e_11, _a;
    };
    Project.fromSisenseResponseArray = function (json) {
        json = json || {};
        var headers = json.headers || [];
        var values = json.values || [];
        var projects = [];
        try {
            for (var values_3 = __values(values), values_3_1 = values_3.next(); !values_3_1.done; values_3_1 = values_3.next()) {
                var value = values_3_1.value;
                projects.push(Project.fromSisenseEntry(headers, value));
            }
        }
        catch (e_12_1) { e_12 = { error: e_12_1 }; }
        finally {
            try {
                if (values_3_1 && !values_3_1.done && (_a = values_3.return)) _a.call(values_3);
            }
            finally { if (e_12) throw e_12.error; }
        }
        return projects;
        var e_12, _a;
    };
    Project.buildImageFilename = function (proj) {
        var filename = environment.baseImageUrl || 'https://images.illuminations.bible/';
        filename += proj.clusterId;
        return filename;
    };
    Object.defineProperty(Project.prototype, "cartId", {
        get: function () {
            return "p" + this.campaignId;
        },
        enumerable: true,
        configurable: true
    });
    Project.prototype.copy = function () {
        var newProj = new Project();
        var keys = Object.getOwnPropertyNames(this);
        try {
            for (var keys_2 = __values(keys), keys_2_1 = keys_2.next(); !keys_2_1.done; keys_2_1 = keys_2.next()) {
                var key = keys_2_1.value;
                newProj[key] = this[key];
            }
        }
        catch (e_13_1) { e_13 = { error: e_13_1 }; }
        finally {
            try {
                if (keys_2_1 && !keys_2_1.done && (_a = keys_2.return)) _a.call(keys_2);
            }
            finally { if (e_13) throw e_13.error; }
        }
        return newProj;
        var e_13, _a;
    };
    Project.prototype.toJson = function () {
        return JSON.stringify(this, Object.getOwnPropertyNames(this));
    };
    return Project;
}());

export { SERVICE_AUDIENCE, SisenseApiService, ApiService, cacheOptions, LanguageService, AUTH_STORAGE_KEY, AuthenticationStorageService, LogLevel, LogService, PREFIX, StorageService, GoogleAnalyticsService, BrowserService, Language, Project };
//# sourceMappingURL=ilb-portal.js.map
