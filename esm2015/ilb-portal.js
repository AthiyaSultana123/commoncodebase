import { Injectable } from '@angular/core';
import { RequestMethod, Response } from '@angular/http';
import * as momentImported from 'moment';
import { decode } from 'urlsafe-base64';
import { Subject } from 'rxjs/Subject';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toArray';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const environment = {
    production: false,
    debug: {
        level: 'debug',
        apiCalls: '*',
        noBody: [
            '/api/auth/native/login',
            '/api/donations/history',
            '/api/sisense/sql'
        ]
    },
    services: {
        'profile.illuminations.bible': 'http://localhost:8001/api',
        'donation.illuminations.bible': 'http://localhost:8002/api',
        'score.seedconnect.com': 'http://localhost:8001/api/sisense'
    },
    stripe: {
        pk: 'pk_test_pjBShAQwejX0t8taYurjIj3S'
    },
    angularGoogleMaps: {
        apiKey: 'AIzaSyBL8yfwUb_VdHh3Mt7CKykCsoyRLSxPR9g'
    },
    trace: 'default environment',
    baseImageUrl: 'https://images.illuminations.bible/'
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} err
 * @return {?}
 */
function errorToJsonString(err) {
    return JSON.stringify(err, ['message', 'arguments', 'type', 'name', 'stack']);
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BrowserService {
    /**
     * @return {?}
     */
    get document() {
        return document;
    }
    /**
     * @return {?}
     */
    get window() {
        return window;
    }
    /**
     * @return {?}
     */
    get Stripe() {
        return (/** @type {?} */ (window)).Stripe;
    }
    /**
     * @return {?}
     */
    get sessionStorage() {
        return (this.window || {}).sessionStorage || null;
    }
    /**
     * @return {?}
     */
    get localStorage() {
        return (this.window || {}).localStorage || null;
    }
}
BrowserService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
BrowserService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class GoogleAnalyticsService {
    /**
     * @param {?} browserService
     */
    constructor(browserService) {
        this.browserService = browserService;
        this.logToGoogle = false;
        this.ga = this.browserService.window.ga;
        this.logToGoogle = environment.production;
    }
    /**
     * @param {?} description
     * @return {?}
     */
    error(description) {
        if (!this.logToGoogle) {
            return; // log.error already console.logs the error so don't duplicate
        }
        this.ga('send', 'exception', {
            exDescription: description
        });
    }
    /**
     * @param {?} newRoute
     * @return {?}
     */
    pageView(newRoute) {
        if (!this.logToGoogle) {
            this.debug(`[GoogleAnalytics] send:pageview:${newRoute}`);
            return;
        }
        this.ga('send', 'pageview', newRoute);
    }
    /**
     * @param {?} eventData
     * @return {?}
     */
    languageSearchEvent(eventData) {
        let /** @type {?} */ data;
        try {
            data = JSON.stringify({
                continent: eventData.continent,
                country: eventData.country,
                language: eventData.language
            });
        }
        catch (/** @type {?} */ err) {
            const /** @type {?} */ msg = `GoogleAnalyticsService.languageSearchEvent:${errorToJsonString(err)}`;
            this.error(msg);
            this.debug(msg);
            return;
        }
        if (!this.logToGoogle) {
            this.debug(`[GoogleAnalytics] send:event:language:search:${data}`);
            return;
        }
        this.ga('send', {
            hitType: 'event',
            eventCategory: 'language',
            eventAction: 'search',
            eventLabel: data
        });
    }
    /**
     * @param {?} eventData
     * @return {?}
     */
    projectSearchEvent(eventData) {
        let /** @type {?} */ data;
        try {
            data = JSON.stringify({
                continent: eventData.continent,
                country: eventData.country,
                language: eventData.language,
                project: eventData.project,
                population: eventData.population,
                remainingNeed: eventData.remainNeed
            });
        }
        catch (/** @type {?} */ err) {
            const /** @type {?} */ msg = `GoogleAnalyticsService.projectSearchEvent:${errorToJsonString(err)}`;
            this.error(msg);
            this.debug(msg);
            return;
        }
        if (!this.logToGoogle) {
            this.debug(`[GoogleAnalytics] send:event:project:search:${data}`);
            return;
        }
        this.ga('send', {
            hitType: 'event',
            eventCategory: 'project',
            eventAction: 'search',
            eventLabel: data
        });
    }
    /**
     * @param {?} msg
     * @param {...?} parts
     * @return {?}
     */
    debug(msg, ...parts) {
        // can't use LogService since it uses GoogleAnalyticsService, which would cause a circular dependency
        if ((environment.debug || /** @type {?} */ ({})).level !== 'debug') {
            return;
        }
        // tslint:disable-next-line:no-console
        console.log(`[debug] ${msg}`, ...parts);
    }
}
GoogleAnalyticsService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
GoogleAnalyticsService.ctorParameters = () => [
    { type: BrowserService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const endpointRegex = new RegExp(`^[^#]*?://.*?(/.*)$`);
/** @enum {number} */
const LogLevel = {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
};
LogLevel[LogLevel.debug] = "debug";
LogLevel[LogLevel.info] = "info";
LogLevel[LogLevel.warn] = "warn";
LogLevel[LogLevel.error] = "error";
class LogService {
    /**
     * @param {?=} ga
     */
    constructor(ga) {
        this.ga = ga;
        this.logLevel = LogLevel.info;
        if (!this.ga) {
            this.ga = /** @type {?} */ ({
                /**
                 * @return {?}
                 */
                error() {
                    // noop
                }
            });
        }
        const /** @type {?} */ logLevel = (environment.debug || /** @type {?} */ ({})).level;
        this.logLevel = /** @type {?} */ (LogLevel[logLevel || 'warn']);
        if (this.logLevel === undefined) {
            const /** @type {?} */ values = [];
            Object.keys(LogLevel).forEach((key) => values.push(key));
            // tslint:disable-next-line
            console.log(`[error] logging system set to invalid log level (${logLevel}). Valid values are: ` +
                `${values.slice(values.length / 2).join(', ')}. Setting to warn.`);
            this.logLevel = LogLevel.warn;
        }
    }
    /**
     * @param {?} msg
     * @param {...?} parts
     * @return {?}
     */
    debug(msg, ...parts) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        // tslint:disable-next-line:no-console
        console.log(`[debug] ${msg}`, ...parts);
    }
    /**
     * @param {?} obj
     * @param {?=} msg
     * @param {...?} parts
     * @return {?}
     */
    debugJson(obj, msg, ...parts) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        try {
            const /** @type {?} */ json = JSON.stringify(obj, null, 2);
            const /** @type {?} */ disp = (msg) ? `${msg}:\n${json}` : json;
            // tslint:disable-next-line:no-console
            console.log(`[debug] ${disp}`, ...parts);
        }
        catch (/** @type {?} */ err) {
            // tslint:disable-next-line:no-console
            console.log(`[debug] %o\n${msg}:\n.debugJson encountered error: ${err}`, obj, ...parts);
        }
    }
    /**
     * Used to debug API calls as the application is running. This can be used anywhere, but it's integrated into
     * `api.service.ts`. To enable logging, set your environment to:
     * {
     *  debug: {
     *    apiCalls: '*'    // this will log ALL api calls
     *  }
     * }
     *
     * To log specific api calls:
     * {
     *  debug: {
     *    apiCalls: ['/echo'],
     *  }
     * }
     *
     * To suppress certain bodies on responses (because they're too noisy):
     * {
     *  debug: {
     *    apiCalls: '*',
     *    noBody: ['/some/really/noisy/path']
     *  }
     * }
     *
     * You can also use `noBody: '*'` to suppress all bodies from being displayed in the response debug log
     *
     * debugApiCall strips the query string, so your apiCalls should only be the actual endpoint. For example,
     * '/echo' if you want to debugApiCalls to '/echo?value=hi&status=277'
     *
     * @param {?} path
     * @param {?} source should be set to the class of the service making the API call. For example, with
     * AuthenticationService, your constructor should be:
     *    constructor(..., private api:ProfileService, ...) {
     *      api.source = this;
     *    }
     *
     * Otherwise, you'll end up with the source being: 'source not set'.
     *
     * @param {?} options
     * @return {?}
     */
    debugApiCall(path, source, options) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        this.logApiCall(path, source, options, 'call');
    }
    /**
     * Same as debugApiCall, but displays the API call response
     *
     * @param {?} path the url that was called that led to the response
     * @param {?} source
     * @param {?} resp an \@angular/http Response object
     * @return {?}
     */
    debugApiResponse(path, source, resp) {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        let /** @type {?} */ body;
        if (resp) {
            try {
                body = resp.json();
            }
            catch (/** @type {?} */ err) {
                body = null;
            }
        }
        this.logApiCall(path, source, { body: body }, 'response');
    }
    /**
     * @param {?} msg
     * @param {...?} parts
     * @return {?}
     */
    info(msg, ...parts) {
        if (this.logLevel > LogLevel.info) {
            return;
        }
        // tslint:disable-next-line:no-console
        console.log(`[info] ${msg}`, ...msg);
    }
    /**
     * @param {?} msg
     * @param {...?} parts
     * @return {?}
     */
    warn(msg, ...parts) {
        if (this.logLevel > LogLevel.warn) {
            return;
        }
        // tslint:disable-next-line:no-console
        console.log(`[warn] ${msg}`, ...msg);
    }
    /**
     * @param {?} err
     * @param {?=} msg
     * @param {...?} parts
     * @return {?}
     */
    error(err, msg, ...parts) {
        let /** @type {?} */ message;
        if (err instanceof Response) {
            if (msg) {
                message = `[error (Response)] ${msg}: code: ${err.status}, ${err.statusText}, url: ${err.url}`;
            }
            else {
                message = `[error (Response)] code: ${err.status}, ${err.statusText}, url: ${err.url}`;
            }
        }
        else {
            if (msg) {
                message = `[error] ${msg}: ${err.message || 'no error message'}\n${err.stack || 'no call stack'}`;
            }
            else {
                message = `[error] ${err.message || 'no error message'} ${err.stack || 'no call stack'}`;
            }
        }
        // tslint:disable-next-line:no-console
        console.log(message, ...parts);
        this.ga.error(message);
    }
    /**
     * @param {?} path
     * @param {?} source
     * @param {?} options
     * @param {?} type
     * @return {?}
     */
    logApiCall(path, source, options, type) {
        const /** @type {?} */ apiCalls = (environment.debug || /** @type {?} */ ({})).apiCalls;
        const /** @type {?} */ noBody = (environment.debug || /** @type {?} */ ({})).noBody;
        if (!apiCalls) {
            return;
        }
        const /** @type {?} */ matches = (typeof path === 'string') ? endpointRegex.exec(path) : '';
        const /** @type {?} */ rawEndpoint = ((matches || []).length > 1) ? matches[1] : '';
        const /** @type {?} */ endPoint = rawEndpoint.split('?')[0];
        if (apiCalls !== '*' && !(Array.isArray(apiCalls) && apiCalls.indexOf(endPoint) > -1)) {
            return;
        }
        source = (source || /** @type {?} */ ({})).name
            || ((source || /** @type {?} */ ({})).constructor || /** @type {?} */ ({})).name
            || source;
        let /** @type {?} */ method;
        if (typeof options.method === 'string') {
            method = options.method.toUpperCase();
        }
        else {
            switch (options.method) {
                case RequestMethod.Get:
                    method = 'GET';
                    break;
                case RequestMethod.Post:
                    method = 'POST';
                    break;
                case RequestMethod.Put:
                    method = 'PUT';
                    break;
                case RequestMethod.Delete:
                    method = 'DELETE';
                    break;
                case RequestMethod.Options:
                    method = 'OPTIONS';
                    break;
                case RequestMethod.Head:
                    method = 'HEAD';
                    break;
                case RequestMethod.Patch:
                    method = 'PATCH';
                    break;
                default:
                    method = (type === 'call')
                        ? 'UNKNOWN_METHOD-' + options.method
                        : null;
                    break;
            }
        }
        let /** @type {?} */ json = '{suppressed by debug config}';
        if (!noBody || (noBody !== '*' && (Array.isArray(noBody) && noBody.indexOf(endPoint) === -1))) {
            try {
                json = options.body ? JSON.stringify(options.body, null, 2) : options.body;
            }
            catch (/** @type {?} */ err) {
                json = `non-json-compliant-response: ${options.body}`;
            }
        }
        // tslint:disable-next-line:no-console
        console.log(`[debug-api-${type}] source: ${source}, ${method || ''}${method ? ':' : ''}${path}, body: ${json}`);
    }
}
LogService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
LogService.ctorParameters = () => [
    { type: GoogleAnalyticsService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ObjectId {
    /**
     * @param {?} id
     */
    constructor(id) {
        this.id = '';
        this.id = id || '';
    }
    /**
     * @return {?}
     */
    get timeStamp() {
        return new Date(parseInt(this.id.substring(0, 8), 16) * 1000);
    }
    /**
     * @return {?}
     */
    toString() {
        return this.id;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ServerDate {
    /**
     * @param {?} seconds
     * @return {?}
     */
    static fromEpochSeconds(seconds) {
        const /** @type {?} */ d = new Date(0);
        d.setUTCSeconds(seconds);
        return d;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class Address {
    constructor() {
    }
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        json = json || {};
        const /** @type {?} */ obj = new Address();
        obj.address1 = json.address1 || json.addressLine1 || '';
        obj.address2 = json.address2 || json.addressLine2 || '';
        obj.city = json.city || json.addressCity || '';
        obj.country = json.country || '';
        obj.state = json.state || json.addressState || '';
        obj.zip = json.zip || json.addressZip || '';
        return obj;
    }
    /**
     * @param {?} jsons
     * @return {?}
     */
    static fromJsonArray(jsons) {
        const /** @type {?} */ results = [];
        if (!Array.isArray(jsons)) {
            for (const /** @type {?} */ json of [jsons]) {
                results.push(Address.fromJson(json));
            }
        }
        return results;
    }
}
class BillingDetails {
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        json = json || {};
        const /** @type {?} */ obj = new BillingDetails();
        obj.brand = json.brand || '';
        obj.expiresMonth = json.expiresMonth || '';
        obj.expiresYear = json.expiresYear || '';
        obj.last4 = json.last4 || '';
        obj.name = json.name || '';
        return obj;
    }
}
class User {
    constructor() {
    }
    /**
     * @return {?}
     */
    get fullName() {
        return `${this.firstName || ''}${this.lastName && this.lastName !== '' ? ' ' : ''}${this.lastName || ''}`;
    }
    /**
     * @return {?}
     */
    get created() {
        return (this.id || /** @type {?} */ ({})).timeStamp || null;
    }
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        json = json || {};
        const /** @type {?} */ obj = new User();
        obj.id = json.id;
        obj.firstName = json.firstName;
        obj.lastName = json.lastName;
        obj.email = json.email;
        obj.phone = json.phone;
        obj.mailingAddress = Address.fromJson(json.mailingAddress);
        obj.billingAddress = Address.fromJson(json.billingAddress);
        obj.billingDetails = BillingDetails.fromJson(json.billingDetails);
        return obj;
    }
    /**
     * @param {?} jsons
     * @return {?}
     */
    static fromJsonArray(jsons) {
        const /** @type {?} */ results = [];
        if (!Array.isArray(jsons)) {
            for (const /** @type {?} */ json of [jsons]) {
                results.push(User.fromJson(json));
            }
        }
        return results;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const moment = momentImported;
class AuthenticationToken {
    /**
     * @param {?} userId
     * @param {?} email
     * @param {?} phone
     * @param {?} domain
     * @param {?} firstName
     * @param {?} lastName
     * @param {?} issued
     * @param {?} expires
     * @param {?} audience
     * @param {?} issuer
     * @param {?} jwtId
     * @param {?} jwtToken
     * @param {?} key
     */
    constructor(userId, email, phone, domain, firstName, lastName, issued, expires, audience, issuer, jwtId, jwtToken, key) {
        this.userId = userId;
        this.email = email;
        this.phone = phone;
        this.domain = domain;
        this.firstName = firstName;
        this.lastName = lastName;
        this.issued = issued;
        this.expires = expires;
        this.audience = audience;
        this.issuer = issuer;
        this.jwtId = jwtId;
        this.jwtToken = jwtToken;
        this.key = key;
    }
    /**
     * @return {?}
     */
    get userCreated() {
        return (this.userId || /** @type {?} */ ({})).timeStamp;
    }
    /**
     * @return {?}
     */
    get expired() {
        return moment().isAfter(this.expires);
    }
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        json = json || {};
        return new AuthenticationToken(new ObjectId(json.id || (json.userId || /** @type {?} */ ({})).id), json.email, json.phone, json.domain, json.firstName, json.lastName, ServerDate.fromEpochSeconds(json.iat), ServerDate.fromEpochSeconds(json.exp), json.aud, json.iss, json.jti, json.token || json.jwtToken, json.key);
    }
    /**
     * @param {?} jsons
     * @return {?}
     */
    static fromJsonArray(jsons) {
        const /** @type {?} */ results = [];
        if (Array.isArray(jsons)) {
            for (const /** @type {?} */ json of jsons) {
                results.push(AuthenticationToken.fromJson(json));
            }
        }
        return results;
    }
    /**
     * From a Json Web Token
     * @param {?} key
     * @param {?} jwt
     * @return {?}
     */
    static fromJwt(key, jwt) {
        jwt = jwt || '';
        let /** @type {?} */ jwtPayload;
        try {
            const /** @type {?} */ jsonParts = jwt.split('.');
            if (jsonParts.length !== 3) {
                throw new Error(`invalid JWT token, should have 3 parts, but had ${jsonParts.length}`);
            }
            const /** @type {?} */ decoded = decode(jsonParts[1]).toString();
            jwtPayload = JSON.parse(decoded);
        }
        catch (/** @type {?} */ err) {
            new LogService().error(err, 'problem with decoding AuthenticationToken from json');
            new LogService().debugJson(jwt, 'errant auth token');
        }
        jwtPayload.token = jwt;
        jwtPayload.key = key;
        return AuthenticationToken.fromJson(jwtPayload);
    }
    /**
     * A token map is returned from the authentication authority:
     * {
     *    token: {
     *      "audience-id-1":"jwt_token-1",
     *      "audience-id-2":"jwt_token-2"
     *    }
     * }
     * @param {?} tokenMap
     * @return {?}
     */
    static fromTokenMap(tokenMap) {
        const /** @type {?} */ tokens = (tokenMap || { token: {} }).token;
        const /** @type {?} */ results = [];
        for (const /** @type {?} */ key of Object.keys(tokens)) {
            results.push(AuthenticationToken.fromJwt(key, tokens[key]));
        }
        return results;
    }
    /**
     * @return {?}
     */
    toUser() {
        return User.fromJson({
            id: this.userId,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            phone: this.phone
        });
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const PREFIX = 'app';
/**
 * Handles local and session storage.
 * For example, to set a session value:
 *    storage.session.setItem('someKey', 'someValue');
 *
 * To store an object (must be JSON.stingifyable):
 *    storage.session.setJson('someKey', {someObject: true});
 *
 * You can also subscribe to an observable for that item:
 *    let someKey$ = storage.session.getObservable('someKey');
 *    someKey$.subscribe({
 *      next: (value) => console.log(value);
 *    });
 *
 *    Once you have an observable, if you call setItem(...) or setJson(...)
 *    the value of the observable's next will be either a string or an Object accordingly.
 *
 * You can clear your storage:
 *    storage.session.clear();
 *
 * You can remove an item:
 *    storage.session.removeItem('someKey');
 *
 *    Note: this will call complete() on the Subject which will cause
 *    complete to fire on the Observables and the Subject will then be deleted.
 *    If you then set a value with the same key, a new Subject will be created
 *    so you need to also get new Observables for it.
 *
 *    As mentioned above, you can call setItem(...) and setJson(...) as many times
 *    as you want, and that will fire the Observables.
 *
 * To change the prefix for the keys, change the storage.prefix value to the string you want.
 */
class StorageService {
    /**
     * @param {?} browserService
     */
    constructor(browserService) {
        this.browserService = browserService;
        this.subjects = {
            local: {},
            session: {},
            property: (type) => {
                return this.subjects[this.property(type)];
            }
        };
        this.types = ['local', 'session'];
        this.store = {
            local: null,
            session: null
        };
        this.local = {
            clear: () => {
                this.clear(0 /* Local */);
            },
            getItem: (key) => {
                return this.getItem(key, 0 /* Local */);
            },
            getJson: (key) => {
                return this.getJson(key, 0 /* Local */);
            },
            getObserver: (key) => {
                return this.getObserver(key, 0 /* Local */);
            },
            key: (n) => {
                return this.key(n, 0 /* Local */);
            },
            length: () => {
                return this.length(0 /* Local */);
            },
            removeItem: (key) => {
                this.removeItem(key, 0 /* Local */);
            },
            removeParentItem: (key) => {
                this.removeParentItem(key, 0 /* Local */);
            },
            setItem: (key, value) => {
                this.setItem(key, value, 0 /* Local */);
            },
            setJson: (key, value) => {
                this.setJson(key, value, 0 /* Local */);
            },
            setParentJson: (key, value) => {
                this.setParentJson(key, value, 0 /* Local */);
            }
        };
        this.session = {
            clear: () => {
                this.clear(1 /* Session */);
            },
            getItem: (key) => {
                return this.getItem(key, 1 /* Session */);
            },
            getJson: (key) => {
                return this.getJson(key, 1 /* Session */);
            },
            getObserver: (key) => {
                return this.getObserver(key, 1 /* Session */);
            },
            key: (n) => {
                return this.key(n, 1 /* Session */);
            },
            length: () => {
                return this.length(1 /* Session */);
            },
            removeItem: (key) => {
                this.removeItem(key, 1 /* Session */);
            },
            removeParentItem: (key) => {
                this.removeParentItem(key, 0 /* Local */);
            },
            setItem: (key, value) => {
                this.setItem(key, value, 1 /* Session */);
            },
            setJson: (key, value) => {
                this.setJson(key, value, 1 /* Session */);
            },
            setParentJson: (key, value) => {
                this.setParentJson(key, value, 1 /* Session */);
            }
        };
        this.store.local = browserService.localStorage;
        this.store.session = browserService.sessionStorage;
    }
    /**
     * @param {?} key
     * @return {?}
     */
    buildKey(key) {
        return `${PREFIX}-${key}`;
    }
    /**
     * @param {?} type
     * @return {?}
     */
    clear(type) {
        this.engine(type).clear();
        const /** @type {?} */ subjects = this.subjects.property(type);
        for (const /** @type {?} */ key in subjects) {
            if (subjects.hasOwnProperty(key)) {
                subjects[key].complete();
            }
        }
        this.subjects[this.property(type)] = {};
    }
    /**
     * @param {?} type
     * @return {?}
     */
    engine(type) {
        return this.store[this.property(type)];
    }
    /**
     * @param {?} key
     * @param {?} type
     * @return {?}
     */
    getItem(key, type) {
        return this.engine(type).getItem(this.buildKey(key));
    }
    /**
     * @param {?} key
     * @param {?} type
     * @return {?}
     */
    getJson(key, type) {
        return JSON.parse(this.engine(type).getItem(this.buildKey(key)));
    }
    /**
     * @template T
     * @param {?} key
     * @param {?} type
     * @return {?}
     */
    getObserver(key, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        return this.subjects.property(type)[key].asObservable();
    }
    /**
     * @param {?} n
     * @param {?} type
     * @return {?}
     */
    key(n, type) {
        return this.engine(type).key(n);
    }
    /**
     * @param {?} type
     * @return {?}
     */
    length(type) {
        return this.engine(type).length;
    }
    /**
     * @param {?} type
     * @return {?}
     */
    property(type) {
        return this.types[type];
    }
    /**
     * @param {?} key
     * @param {?} type
     * @return {?}
     */
    removeItem(key, type) {
        this.engine(type).removeItem(this.buildKey(key));
        if (this.subjects.property(type)[key]) {
            this.subjects.property(type)[key].complete();
        }
        delete this.subjects[this.property(type)][key];
    }
    /**
     * @param {?} key
     * @param {?} type
     * @return {?}
     */
    removeParentItem(key, type) {
        if (type === 1 /* Session */) {
            this.browserService.window.opener.sessionStorage.removeItem(this.buildKey(key));
        }
        else {
            this.browserService.window.opener.localStorage.removeItem(this.buildKey(key));
        }
        if (this.subjects.property(type)[key]) {
            this.subjects.property(type)[key].complete();
        }
        delete this.subjects[this.property(type)][key];
    }
    /**
     * @param {?} key
     * @param {?} value
     * @param {?} type
     * @return {?}
     */
    setItem(key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        this.engine(type).setItem(this.buildKey(key), value);
        this.subjects.property(type)[key].next(value);
    }
    /**
     * @param {?} key
     * @param {?} value
     * @param {?} type
     * @return {?}
     */
    setJson(key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        this.engine(type).setItem(this.buildKey(key), JSON.stringify(value));
        this.subjects.property(type)[key].next(value);
    }
    /**
     * @param {?} key
     * @param {?} value
     * @param {?} type
     * @return {?}
     */
    setParentJson(key, value, type) {
        if (!this.subjects[this.property(type)][key]) {
            this.subjects.property(type)[key] = new Subject();
        }
        if (type === 1 /* Session */) {
            this.browserService.window.opener.sessionStorage.setItem(this.buildKey(key), JSON.stringify(value));
        }
        else {
            this.browserService.window.opener.localStorage.setItem(this.buildKey(key), JSON.stringify(value));
        }
        this.subjects.property(type)[key].next(value);
    }
}
StorageService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
StorageService.ctorParameters = () => [
    { type: BrowserService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const AUTH_STORAGE_KEY = 'ilb_auth';
/**
 * Stores and retrieves AuthenticationTokens from Session or Local Storage
 */
class AuthenticationStorageService {
    /**
     * @param {?} store
     * @param {?} log
     */
    constructor(store, log) {
        this.store = store;
        this.log = log;
    }
    /**
     * @param {?} service
     * @return {?}
     */
    getAuthenticationToken(service) {
        const /** @type {?} */ tokensJson = (this.store.local.getItem(AUTH_STORAGE_KEY) || this.store.session.getItem(AUTH_STORAGE_KEY));
        if (!tokensJson) {
            return null;
        }
        let /** @type {?} */ tokens;
        try {
            tokens = JSON.parse(tokensJson);
            if (!Array.isArray(tokens)) {
                this.log.error(new Error('stored tokens should have been in an array'));
            }
        }
        catch (/** @type {?} */ err) {
            this.log.info(err, 'stored auth tokens are corrupted... deleting them from the store');
            this.clearTokens();
            return null;
        }
        for (const /** @type {?} */ token of tokens) {
            if (token.key === service) {
                return AuthenticationToken.fromJson(token);
            }
        }
        return null;
    }
    /**
     * @return {?}
     */
    getAuthenticationTokens() {
        const /** @type {?} */ tokensJson = (this.store.local.getItem(AUTH_STORAGE_KEY) || this.store.session.getItem(AUTH_STORAGE_KEY));
        if (!tokensJson) {
            return null;
        }
        let /** @type {?} */ tokens;
        try {
            tokens = JSON.parse(tokensJson);
            if (!Array.isArray(tokens)) {
                this.log.error(new Error('stored tokens should have been in an array'));
            }
        }
        catch (/** @type {?} */ err) {
            this.log.info(err, 'stored auth tokens are corrupted... deleting them from the store');
            this.clearTokens();
            return null;
        }
        return AuthenticationToken.fromJsonArray(tokens);
    }
    /**
     * @param {?} tokens
     * @param {?} remember
     * @return {?}
     */
    saveTokens(tokens, remember) {
        this.clearTokens();
        const /** @type {?} */ store = (remember) ? this.store.local : this.store.session;
        store.setItem(AUTH_STORAGE_KEY, JSON.stringify(tokens));
    }
    /**
     * @return {?}
     */
    clearTokens() {
        this.store.session.removeItem(AUTH_STORAGE_KEY);
        this.store.local.removeItem(AUTH_STORAGE_KEY);
    }
}
AuthenticationStorageService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AuthenticationStorageService.ctorParameters = () => [
    { type: StorageService, },
    { type: LogService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Developer note:
 *
 * The ApiService was originally implemented using the now legacy Http class (replaced by HttpClient). To
 * minimize the impact of the refactor, the ApiService has maintained its original interface. In the future
 * this may be refactored out to provide a more native experience.
 *
 * @record
 */

/**
 * This interface replaces the deprecated https://angular.io/api/http/RequestOptionsArgs
 * Used in the get, post, etc methods
 * @record
 */

/**
 * @abstract
 */
class ApiService {
    /**
     *
     * @param {?} service This is the name of the service used by the auth system. It is provided by the child class
     * when it calls super as is required when a child class implements ApiService.
     * @param {?} authStorage
     * @param {?} http
     * @param {?} log
     */
    constructor(service, authStorage, http, log) {
        this.service = service;
        this.authStorage = authStorage;
        this.http = http;
        this.log = log;
        this.source = { name: 'source not set' };
        this._authError$ = new Subject();
        this._serverDown$ = new Subject();
        this.baseUrl = `${environment.services[service]}`;
        if (!(environment.services || /** @type {?} */ ({}))[service]) {
            log.error(new Error(`${this.constructor.name} needs configuration 'environment.services.${service}' to be defined `
                + `in order to properly function.`));
        }
    }
    /**
     * emitted when server returns a 401 (passes the authentication token that caused the 401, or null if non was found)
     * @return {?}
     */
    get authError$() {
        return this._authError$.asObservable();
    }
    /**
     * emitted when request returns a 0 (server is down)
     * @return {?}
     */
    get serverDown$() {
        return this._serverDown$.asObservable();
    }
    /**
     * @param {?} endpoint
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    request(endpoint, options, apiOptions) {
        options = (options || { method: 'GET' });
        apiOptions = apiOptions || {};
        // build url
        const /** @type {?} */ url = `${this.baseUrl}${endpoint}`;
        this.log.debugApiCall(url, this.source, options);
        const /** @type {?} */ authToken = this.authStorage.getAuthenticationToken(this.service);
        // build headers
        // if options.headers is provided by integrator, then let those values take precidence, but set
        // 'Authorization' and 'Content-Type' headers to their defaults if not provided.
        const /** @type {?} */ headers = Object.assign({}, options.headers);
        headers['Authorization'] = headers['Authorization']
            || ((authToken) ? `Bearer ${(authToken || (/** @type {?} */ ({}))).jwtToken}` : undefined);
        headers['Content-Type'] = headers['Content-Type'] || 'application/json';
        // any header set that's undefined will cause a "Cannot read property 'length' of undefined" error, which is hard
        // to debug, so spend a few cycles making sure that none of the keys have a value of undefined.
        for (const /** @type {?} */ key of Object.keys(headers)) {
            if (headers[key] === undefined || headers[key] === null) {
                delete headers[key];
            }
        }
        // IRequestOptions limits the headers property to a key/value pair to simplify the immediately preceeding header
        // logic. This is incompatible with the type for `headers` that http.request expects, so cast it to any
        // to prevent a typescript error.
        options.headers = /** @type {?} */ (new HttpHeaders(headers));
        // make request and handle errors
        return this
            .http
            .request(options.method, url, options)
            .do((req) => {
            this.log.debugApiResponse(url, this.source, req);
        })
            .catch((res) => {
            switch (res.status) {
                case 0:
                    this.log.error(res, `>>server down<<: ${res} - if the server is up, make sure this isn't a CORS error`);
                    this._serverDown$.next();
                    return Observable.throw(res);
                case 401:
                    this.log.debug(`authentication error: %o`, res);
                    if (!apiOptions.disableServerDown) {
                        this._authError$.next(authToken);
                    }
                    else {
                        this.log.debug('authError$ not emitted since apiOptions.disableServerDown === true');
                    }
                    return Observable.throw(res);
                case 403:
                    return Observable.throw(res);
                default:
                    this.log.error(res, 'Unexpected Error');
                    return Observable.throw(res);
            }
        });
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    get(url, options, apiOptions) {
        options = options || {};
        options.method = 'GET';
        return this.request(url, options, apiOptions);
    }
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    post(url, body, options, apiOptions) {
        options = options || {};
        options.method = 'POST';
        options.body = body;
        return this.request(url, options, apiOptions);
    }
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    put(url, body, options, apiOptions) {
        options = options || {};
        options.method = 'PUT';
        options.body = body;
        return this.request(url, options, apiOptions);
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    delete(url, options, apiOptions) {
        options = options || {};
        options.method = 'DELETE';
        return this.request(url, options, apiOptions);
    }
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    patch(url, body, options, apiOptions) {
        options = options || {};
        options.method = 'PATCH';
        options.body = body;
        return this.request(url, options, apiOptions);
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @param {?=} apiOptions
     * @return {?}
     */
    head(url, options, apiOptions) {
        options = options || {};
        options.method = 'HEAD';
        return this.request(url, options, apiOptions);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const SERVICE_AUDIENCE = 'score.seedconnect.com';
class SisenseApiService extends ApiService {
    /**
     * @param {?} authStorage
     * @param {?} http
     * @param {?} log
     */
    constructor(authStorage, http, log) {
        if (!environment.services || !environment.services[SERVICE_AUDIENCE]) {
            throw new Error(`environment.services is misconfigured for SisenseApiService, expecting key ${SERVICE_AUDIENCE}`);
        }
        super(SERVICE_AUDIENCE, authStorage, http, log);
    }
}
SisenseApiService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
SisenseApiService.ctorParameters = () => [
    { type: AuthenticationStorageService, },
    { type: HttpClient, },
    { type: LogService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class Language {
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        const /** @type {?} */ lang = new Language();
        lang.continent = json.Continent || json.continent || '';
        lang.country = json.Country || json.country || '';
        lang.latitude = json.Latitude || json.latitude || 0;
        lang.longitude = json.Longitude || json.longitude || 0;
        lang.languageName = json.LanguageName || json.languageName || '';
        lang.languageId = json.LanguageID || json.languageId || '';
        lang.campaignId = json.CampaignID || json.campaignId || '';
        lang.population = Number.isInteger(json.Population) ? json.Population : Number.parseInt(json.Population);
        lang.populationRange = json.PopulationRange || json.populationRange || '';
        lang.beginYear = json.BeginYr || json.beginYr || 0;
        lang.projectCount = json.ProjectCount || json.projectCount || 0;
        lang.orgCount = json.OrgCount || json.orgCount || 0;
        lang.featuredLanguageFlag = (json.FeaturedLanguageFlag === 'Y') || json.featuredLanguageFlag || false;
        lang.isConfidentialLanguageFlag = (json.IsConfidentialLanguageFlag === 'Y') || json.isConfidentialLanguageFlag || false;
        lang.isSignLanguageFlag = (json.IsSignLanguageFlag === 'Y') || json.isSignLanguageFlag || false;
        lang.projectSensitivityLevel = json.ProjectSensitivityLevel || json.projectSensitivityLevel || 0;
        lang.primaryReligion = json.PrimaryReligion || json.primaryReligion || '';
        lang.prayerCommitCount = json.PrayerCommitCnt || json.prayerCommitCount || 0;
        lang.commonFrameworkFlag = (json.CommonFrameworkFlag === 'Y') || json.commonFrameworkFlag || false;
        lang.firstScriptureFlag = (json.FirstScriptureFlag === 'Y') || json.firstScriptureFlag || false;
        lang.joshuaProjectLeastReachedFlag = (json.JoshuaProjectLeastReachedFlag === 'Y')
            || json.commonFrameworkFlag || false;
        lang.completedFullBibleFlag = (json.CompletedFullBibleFlag === 'Y') || json.completedFullBibleFlag || false;
        lang.completedNewTestamentFlag = (json.CompletedNewTestamentFlag === 'Y') || json.completedNewTestamentFlag || false;
        lang.completedJesusFilmFlag = (json.CompletedJesusFilmFlag === 'Y') || json.completedJesusFilmFlag || false;
        lang.completedAudioRecordingsFlag = (json.CompletedAudioRecordingsFlag === 'Y')
            || json.completedAudioRecordingsFlag || false;
        lang.completed25ChaptersFlag = (json.Completed25ChaptersFlag === 'Y') || json.completed25ChaptersFlag || false;
        lang.translationInProgressFlag = (json.TranslationInProgressFlag === 'Y') || json.translationInProgressFlag || false;
        lang.noTranslationYetFlag = (json.NoTranslationYetFlag === 'Y') || json.noTranslationYetFlag || false;
        lang.translationTypeWrittenTranslationFlag = (json.TranslationTypeWrittenTranslationFlag === 'Y')
            || json.translationTypeWrittenTranslationFlag || false;
        lang.translationTypeWrittenStoriesFlag = (json.TranslationTypeWrittenStoriesFlag === 'Y')
            || json.translationTypeWrittenStoriesFlag || false;
        lang.translationTypeOralTranslationFlag = (json.TranslationTypeOralTranslationFlag === 'Y')
            || json.translationTypeOralTranslationFlag || false;
        lang.translationTypeOralStoryingFlag = (json.TranslationTypeOralStoryingFlag === 'Y')
            || json.translationTypeOralStoryingFlag || false;
        lang.translationTypeSignLanguageFlag = (json.TranslationTypeSignLanguageFlag === 'Y')
            || json.translationTypeSignLanguageFlag || false;
        lang.translationTypeFilmFlag = (json.TranslationTypeFilmFlag === 'Y') || json.translationTypeFilmFlag || false;
        lang.translationTypeOtherFlag = (json.TranslationTypeOtherFlag === 'Y') || json.translationTypeOtherFlag || false;
        lang.goalsFullBibleFlag = (json.GoalsFullBibleFlag === 'Y') || json.goalsFullBibleFlag || false;
        lang.goalsFullNTFlag = (json.GoalsFullNTFlag === 'Y') || json.goalsFullNTFlag || false;
        lang.goalsFullOTFlag = (json.GoalsFullOTFlag === 'Y') || json.goalsFullOTFlag || false;
        lang.goalsAGospelFlag = (json.GoalsAGospelFlag === 'Y') || json.goalsAGospelFlag || false;
        lang.goalsGenesisFlag = (json.GoalsGenesisFlag === 'Y') || json.goalsGenesisFlag || false;
        lang.goalsNtPortionsFlag = (json.GoalsNTPortionsFlag === 'Y') || json.goalsNtPortionsFlag || false;
        lang.goalsOtPortionsFlag = (json.GoalsOTPortionsFlag === 'Y') || json.goalsOtPortionsFlag || false;
        lang.goalsJesusFilmFlag = (json.GoalsJesusFilmFlag === 'Y') || json.goalsJesusFilmFlag || false;
        lang.goalsBibleStoriesFlag = (json.GoalsBibleStoriesFlag === 'Y') || json.goalsBibleStoriesFlag || false;
        lang.langLifetimeFundingGoal = json.LangLifetimeFundingGoal || json.langLifetimeFundingGoal || 0;
        lang.langLifetimeRemainNeed = Math.max(0, json.LangLifetimeRemainNeed || 0, json.langLifetimeRemainNeed || 0);
        lang.beforeIBibleDonationsLangLifetimeRemainNeed = Math.max(0, json.BeforeIBibleDonationsLangLifetimeRemainNeed || 0, json.beforeIBibleDonationsLangLifetimeRemainNeed || 0);
        lang.totalLanguagePledges = json.TotalLanguagePledges || json.langLifetimeFundingGoal || 0;
        lang.totalLanguageGiving = json.TotalLanguageGiving || json.langLifetimeFundingGoal || 0;
        lang.beforeIBibleTotalLanguageGiving = json.BeforeIBibleTotalLanguageGiving || json.langLifetimeFundingGoal || 0;
        lang.iBibleOnlyTotalRemainingNeed = Math.max(0, json.IBibleOnlyTotalRemainingNeed || 0, json.iBibleOnlyTotalRemainingNeed || 0);
        lang.imageSrc = json.ImageSrc || json.imageSrc || '';
        lang.lowResImageSrc = json.LowResImageSrc || json.lowResImageSrc || '';
        lang.description = json.Description || json.description || '';
        lang.gathering2017Flag = (json.Gathering2017Flag === 'Y') || json.gathering2017Flag || false;
        lang.visibleFlag = (json.VisibleFlag === 'Y') || json.visibleFlag || false;
        lang.region = json.Region || json.region || '';
        lang.totalDonors = json.TotalDonors || json.totalDonors || 0;
        return lang;
    }
    /**
     * @param {?} headers
     * @param {?} values
     * @return {?}
     */
    static fromSisenseEntry(headers, values) {
        const /** @type {?} */ json = {};
        let /** @type {?} */ i = 0;
        for (const /** @type {?} */ key of headers) {
            json[key] = values[i];
            i++;
        }
        return Language.fromJson(json);
    }
    /**
     * @param {?} json
     * @return {?}
     */
    static fromSisenseResponseArray(json) {
        json = json || {};
        const /** @type {?} */ headers = json.headers || [];
        const /** @type {?} */ values = json.values || [];
        const /** @type {?} */ languages = [];
        for (const /** @type {?} */ value of values) {
            languages.push(Language.fromSisenseEntry(headers, value));
        }
        return languages;
    }
    /**
     * @return {?}
     */
    get bannerImage() {
        return this.buildImageFilename + '-1440x800.png';
    }
    /**
     * @return {?}
     */
    get cardImage() {
        return this.buildImageFilename + '-404x210.png';
    }
    /**
     * @return {?}
     */
    get buildImageFilename() {
        let /** @type {?} */ filename = environment.baseImageUrl || 'https://images.illuminations.bible/';
        if (this.region === 'Northern America') {
            filename += 'North-America';
        }
        else {
            filename += this.continent;
        }
        return filename;
    }
    /**
     * @return {?}
     */
    get cartId() {
        // for ICartId
        return this.languageId.toLowerCase();
    }
    /**
     * @return {?}
     */
    copy() {
        const /** @type {?} */ newLang = new Language();
        const /** @type {?} */ keys = Object.getOwnPropertyNames(this);
        for (const /** @type {?} */ key of keys) {
            newLang[key] = this[key];
        }
        return newLang;
    }
    /**
     * @return {?}
     */
    toJson() {
        return JSON.stringify(this, Object.getOwnPropertyNames(this));
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let cacheOptions = {
    cacheLife: 1000 * 60 * 5 // 5 min
};
class LanguageService {
    /**
     * @param {?} api
     */
    constructor(api) {
        this.api = api;
        this.projectSearchFlag = false;
    }
    /**
     * @return {?}
     */
    getContinents() {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .pluck('continent')
            .distinct((c) => c)
            .toArray();
    }
    /**
     * @return {?}
     */
    getCountries() {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .pluck('country')
            .distinct((c) => c)
            .toArray();
    }
    /**
     * @param {?} continent
     * @return {?}
     */
    getCountriesByContinent(continent) {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .filter((language) => language.continent.toLowerCase() === continent.toLowerCase())
            .pluck('country')
            .distinct((c) => c)
            .toArray();
    }
    /**
     * @return {?}
     */
    getFeaturedLanguages() {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .filter((language) => language.featuredLanguageFlag)
            .toArray();
    }
    /**
     * @return {?}
     */
    getFilteredLanguages() {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .filter((language) => (language.gathering2017Flag))
            .toArray();
    }
    /**
     * @param {?} languageId
     * @return {?}
     */
    getLanguageById(languageId) {
        return this
            .getLanguages()
            .concatMap((languages) => {
            return Observable
                .from(languages)
                .first((language) => language.languageId === languageId, null, null);
        });
    }
    /**
     * @return {?}
     */
    getLanguageNames() {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .pluck('languageName')
            .distinct((ln) => ln)
            .toArray();
    }
    /**
     * @return {?}
     */
    getLanguages() {
        if (!this.cache$) {
            this
                .cache$ = Observable
                .defer(() => {
                return this
                    .api
                    .get('/sql?query=select%20*%20from%20Languages')
                    .concatMap((json) => {
                    const /** @type {?} */ languages = Language.fromSisenseResponseArray(json);
                    const /** @type {?} */ results = [];
                    for (const /** @type {?} */ language of languages) {
                        if (language.visibleFlag) {
                            results.push(language);
                        }
                    }
                    return results;
                })
                    .toArray();
            })
                .publishReplay(1, cacheOptions.cacheLife)
                .refCount()
                .take(1);
        }
        return /** @type {?} */ (this.cache$);
    }
    /**
     * @param {?} continent
     * @return {?}
     */
    getLanguagesByContinent(continent) {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .filter((language) => language.continent.toLowerCase() === continent.toLowerCase())
            .pluck('languageName')
            .distinct((ln) => ln)
            .toArray();
    }
    /**
     * @param {?} country
     * @return {?}
     */
    getLanguagesByCountry(country) {
        return this
            .getLanguages()
            .concatMap((languagesArray) => Observable.from(languagesArray))
            .filter((language) => language.country.toLocaleLowerCase() === country.toLocaleLowerCase())
            .pluck('languageName')
            .distinct((ln) => ln)
            .toArray();
    }
    /**
     * @param {?} language
     * @return {?}
     */
    getTranslationType(language) {
        let /** @type {?} */ translationType = '';
        if (language.translationTypeWrittenTranslationFlag
            || language.translationTypeWrittenStoriesFlag) {
            translationType += 'Written,';
        }
        if (language.translationTypeOralTranslationFlag
            || language.translationTypeOralStoryingFlag) {
            translationType += ' Oral,';
        }
        if (language.translationTypeFilmFlag) {
            translationType += ' Film,';
        }
        if (language.translationTypeOtherFlag) {
            translationType += ' Other,';
        }
        if (language.translationTypeSignLanguageFlag) {
            // If it is a signLanguage project it only shows Sign Language Video, no other type
            translationType = ' Sign Language Video,';
        }
        if (translationType === '') {
            translationType = 'Oral,';
        }
        translationType = translationType.slice(0, -1); // remove trailing commma
        return translationType;
    }
}
LanguageService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
LanguageService.ctorParameters = () => [
    { type: SisenseApiService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class Project {
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJson(json) {
        const /** @type {?} */ project = new Project();
        project.projectId = json.ProjectID || json.projectId || '';
        project.clusterId = json.ClusterID || json.clusterId || '';
        project.campaignId = json.CampaignID || json.clusterId || '';
        project.projectName = json.ProjectName || json.projectName || '';
        project.languageId = json.LanguageID || json.languageId || '';
        project.stgStatusName = json.StgStatusName || json.stgStatusName || '';
        project.pipelineFlag = (json.PipelineFlag === 'Y') || json.pipelineFlag || false;
        project.organizationId = json.OrganizationID || json.organizationId || '';
        project.projectLifetimeFundingGoal = json.ProjectLifetimeFundingGoal
            || json.projectLifetimeFundingGoal || 0;
        project.projectLifetimeRemainNeed = Math.max(0, json.ProjectLifetimeRemainNeed
            || json.projectLifetimeRemainNeed || 0);
        project.beforeIBibleDonationsRemainingNeed = Math.max(0, json.BeforeIBibleDonationsRemainingNeed || 0, json.beforeIBibleDonationsRemainingNeed || 0);
        project.totalPledges = json.TotalPledges || json.totalPledges || 0;
        project.totalGiven = json.TotalGiven || json.totalGiven || 0;
        project.beforeIBibleTotalGiven = json.BeforeIBibleTotalGiven || json.beforeIBibleTotalGiven || 0;
        project.iBibleOnlyTotalGiven = json.IBibleOnlyTotalGiven || json.iBibleOnlyTotalGiven || 0;
        project.languageCount = json.LanguageCount || json.languageCount || 0;
        project.isClusterFlag = json.IsClusterFlag === 'Y';
        project.commonFrameworkFlag = (json.CommonFrameworkFlag === 'Y') || json.commonFrameworkFlag || false;
        project.featuredProjectFlag = (json.FeaturedProjectFlag === 'Y') || json.featuredProjectFlag || false;
        project.prayerCommitCnt = json.PrayerCommitCnt || json.prayerCommitCnt || 0;
        project.beginYr = json.BeginYr || json.beginYr || 0;
        project.sensitivityLevel = json.SensitivityLevel || json.sensitivityLevel || 0;
        project.firstScriptureFlag = (json.FirstScriptureFlag === 'Y') || json.firstScriptureFlag || false;
        project.translationTypeWrittenTranslationFlag = (json.TranslationTypeWrittenTranslationFlag === 'Y')
            || json.translationTypeWrittenTranslationFlag || false;
        project.translationTypeWrittenStoriesFlag = (json.TranslationTypeWrittenStoriesFlag === 'Y')
            || json.translationTypeWrittenStoriesFlag || false;
        project.translationTypeOralTranslationFlag = (json.TranslationTypeOralTranslationFlag === 'Y')
            || json.translationTypeOralTranslationFlag || false;
        project.translationTypeOralStoryingFlag = (json.TranslationTypeOralStoryingFlag === 'Y')
            || json.translationTypeOralStoryingFlag || false;
        project.translationTypeSignLanguageFlag = (json.TranslationTypeSignLanguageFlag === 'Y')
            || json.translationTypeSignLanguageFlag || false;
        project.translationTypeFilmFlag = (json.TranslationTypeFilmFlag === 'Y')
            || json.translationTypeFilmFlag || false;
        project.translationTypeOtherGoalsFlag = (json.TranslationTypeOtherGoalsFlag === 'Y')
            || json.translationTypeOtherFlag || false;
        project.goalsFullBibleFlag = (json.FullBibleFlag === 'Y') || json.goalsFullBibleFlag || false;
        project.goalsFullNTFlag = (json.GoalsFullNTFlag === 'Y') || json.goalsFullNTFlag || false;
        project.goalsFullOTFlag = (json.GoalsFullOTFlag === 'Y') || json.goalsFullOTFlag || false;
        project.goalsAGospelFlag = (json.GoalsAGospelFlag === 'Y') || json.goalsAGospelFlag || false;
        project.goalsGenesisFlag = (json.GoalsGenesisFlag === 'Y') || json.goalsGenesisFlag || false;
        project.goalsNtPortionsFlag = (json.GoalsNTPortionsFlag === 'Y') || json.goalsNtPortionsFlag || false;
        project.goalsOtPortionsFlag = (json.GoalsOTPortionsFlag === 'Y') || json.goalsOtPortionsFlag || false;
        project.goalsJesusFilmFlag = (json.GoalsJesusFilmFlag === 'Y') || json.goalsJesusFilmFlag || false;
        project.goalsBibleStoriesFlag = (json.GoalsBibleStoriesFlag === 'Y') || json.goalsBibleStoriesFlag || false;
        project.gathering2017Flag = (json.Gathering2017Flag === 'Y') || json.gathering2017Flag || false;
        project.visibleFlag = (json.VisibleFlag === 'Y') || json.visibleFlag || false;
        project.population = json.ProjectPopulation || json.population || 0;
        project.populationRange = json.ProjectPopulationRange || json.populationRange || '';
        project.projectDialectFlag = (json.ProjectDialectFlag === 'Y') || json.firstScriptureFlag || false;
        project.description = json.ProjectDescriptio || json.projectDescription || '';
        project.visibleFlag = (json.VisibleFlag === 'Y') || json.firstScriptureFlag || false;
        project.bannerImage = this.buildImageFilename(project) + '-1440x800.png';
        project.cardImage = this.buildImageFilename(project) + '404x210.png';
        project.meta = {};
        project.totalDonors = json.TotalDonors || json.totalDonors || 0;
        project.countryCount = json.countryCount || 1;
        return project;
    }
    /**
     * @param {?} headers
     * @param {?} values
     * @return {?}
     */
    static fromSisenseEntry(headers, values) {
        const /** @type {?} */ json = {};
        let /** @type {?} */ i = 0;
        for (const /** @type {?} */ key of headers) {
            json[key] = values[i];
            i++;
        }
        return Project.fromJson(json);
    }
    /**
     * @param {?} json
     * @return {?}
     */
    static fromSisenseResponseArray(json) {
        json = json || {};
        const /** @type {?} */ headers = json.headers || [];
        const /** @type {?} */ values = json.values || [];
        const /** @type {?} */ projects = [];
        for (const /** @type {?} */ value of values) {
            projects.push(Project.fromSisenseEntry(headers, value));
        }
        return projects;
    }
    /**
     * @param {?} proj
     * @return {?}
     */
    static buildImageFilename(proj) {
        let /** @type {?} */ filename = environment.baseImageUrl || 'https://images.illuminations.bible/';
        filename += proj.clusterId;
        return filename;
    }
    /**
     * @return {?}
     */
    get cartId() {
        return `p${this.campaignId}`;
    }
    /**
     * @return {?}
     */
    copy() {
        const /** @type {?} */ newProj = new Project();
        const /** @type {?} */ keys = Object.getOwnPropertyNames(this);
        for (const /** @type {?} */ key of keys) {
            newProj[key] = this[key];
        }
        return newProj;
    }
    /**
     * @return {?}
     */
    toJson() {
        return JSON.stringify(this, Object.getOwnPropertyNames(this));
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { SERVICE_AUDIENCE, SisenseApiService, ApiService, cacheOptions, LanguageService, AUTH_STORAGE_KEY, AuthenticationStorageService, LogLevel, LogService, PREFIX, StorageService, GoogleAnalyticsService, BrowserService, Language, Project };
//# sourceMappingURL=ilb-portal.js.map
