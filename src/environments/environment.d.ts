export declare const environment: {
    production: boolean;
    debug: {
        level: string;
        apiCalls: string;
        noBody: string[];
    };
    services: {
        'profile.illuminations.bible': string;
        'donation.illuminations.bible': string;
        'score.seedconnect.com': string;
    };
    stripe: {
        pk: string;
    };
    angularGoogleMaps: {
        apiKey: string;
    };
    trace: string;
    baseImageUrl: string;
};
