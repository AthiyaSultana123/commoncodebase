import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toArray';
import { Observable } from 'rxjs/Observable';
import { Language } from '../models/language';
import { SisenseApiService } from './http/sisense-api.service';
import { Project } from '../models/project';
export declare let cacheOptions: {
    cacheLife: number;
};
export declare class LanguageService {
    private api;
    projectSearchFlag: boolean;
    selectedProject: Project;
    private cache$;
    constructor(api: SisenseApiService);
    getContinents(): Observable<{}[]>;
    getCountries(): Observable<{}[]>;
    getCountriesByContinent(continent: string): Observable<{}[]>;
    getFeaturedLanguages(): Observable<Language[]>;
    getFilteredLanguages(): Observable<Language[]>;
    getLanguageById(languageId: any): Observable<Language>;
    getLanguageNames(): Observable<{}[]>;
    getLanguages(): Observable<Language[]>;
    getLanguagesByContinent(continent: string): Observable<{}[]>;
    getLanguagesByCountry(country: string): Observable<{}[]>;
    getTranslationType(language: Language): string;
}
