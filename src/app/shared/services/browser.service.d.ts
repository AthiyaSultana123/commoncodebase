export declare class BrowserService {
    readonly document: any;
    readonly window: any;
    readonly Stripe: any;
    readonly sessionStorage: any;
    readonly localStorage: any;
}
