import { AuthenticationToken } from '../models/authentication-token';
import { LogService } from './log.service';
import { StorageService } from './storage.service';
export declare const AUTH_STORAGE_KEY = "ilb_auth";
/**
 * Stores and retrieves AuthenticationTokens from Session or Local Storage
 */
export declare class AuthenticationStorageService {
    private store;
    private log;
    constructor(store: StorageService, log: LogService);
    getAuthenticationToken(service: string): AuthenticationToken;
    getAuthenticationTokens(): AuthenticationToken[];
    saveTokens(tokens: AuthenticationToken[], remember: boolean): void;
    clearTokens(): void;
}
