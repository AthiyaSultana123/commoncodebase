import { Observable } from 'rxjs/Observable';
import { BrowserService } from './browser.service';
export declare const enum StorageType {
    Local = 0,
    Session = 1,
}
export declare const PREFIX = "app";
/**
 * Handles local and session storage.
 * For example, to set a session value:
 *    storage.session.setItem('someKey', 'someValue');
 *
 * To store an object (must be JSON.stingifyable):
 *    storage.session.setJson('someKey', {someObject: true});
 *
 * You can also subscribe to an observable for that item:
 *    let someKey$ = storage.session.getObservable('someKey');
 *    someKey$.subscribe({
 *      next: (value) => console.log(value);
 *    });
 *
 *    Once you have an observable, if you call setItem(...) or setJson(...)
 *    the value of the observable's next will be either a string or an Object accordingly.
 *
 * You can clear your storage:
 *    storage.session.clear();
 *
 * You can remove an item:
 *    storage.session.removeItem('someKey');
 *
 *    Note: this will call complete() on the Subject which will cause
 *    complete to fire on the Observables and the Subject will then be deleted.
 *    If you then set a value with the same key, a new Subject will be created
 *    so you need to also get new Observables for it.
 *
 *    As mentioned above, you can call setItem(...) and setJson(...) as many times
 *    as you want, and that will fire the Observables.
 *
 * To change the prefix for the keys, change the storage.prefix value to the string you want.
 */
export declare class StorageService {
    private browserService;
    private subjects;
    private types;
    private store;
    local: {
        clear: () => void;
        getItem: (key: string) => string;
        getJson: (key: string) => Object;
        getObserver: <T>(key: string) => Observable<T>;
        key: (n: number) => any;
        length: () => number;
        removeItem: (key: string) => void;
        removeParentItem: (key: string) => void;
        setItem: (key: string, value: string) => void;
        setJson: (key: string, value: any) => void;
        setParentJson: (key: string, value: any) => void;
    };
    session: {
        clear: () => void;
        getItem: (key: string) => string;
        getJson: (key: string) => Object;
        getObserver: <T>(key: string) => Observable<T>;
        key: (n: number) => any;
        length: () => number;
        removeItem: (key: string) => void;
        removeParentItem: (key: string) => void;
        setItem: (key: string, value: string) => void;
        setJson: (key: string, value: any) => void;
        setParentJson: (key: string, value: any) => void;
    };
    constructor(browserService: BrowserService);
    private buildKey(key);
    private clear(type);
    private engine(type);
    private getItem(key, type);
    private getJson(key, type);
    private getObserver<T>(key, type);
    private key(n, type);
    private length(type);
    private property(type);
    private removeItem(key, type);
    private removeParentItem(key, type);
    private setItem(key, value, type);
    private setJson(key, value, type);
    private setParentJson(key, value, type);
}
