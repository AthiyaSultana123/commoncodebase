import { Response } from '@angular/http';
import { GoogleAnalyticsService } from './google-analytics.service';
import { IRequestOptions } from './http/api.service';
export declare enum LogLevel {
    debug = 0,
    info = 1,
    warn = 2,
    error = 3,
}
export declare class LogService {
    private ga;
    logLevel: LogLevel;
    constructor(ga?: GoogleAnalyticsService);
    debug(msg: string | object, ...parts: any[]): void;
    debugJson(obj: any, msg?: string, ...parts: any[]): void;
    /**
     * Used to debug API calls as the application is running. This can be used anywhere, but it's integrated into
     * `api.service.ts`. To enable logging, set your environment to:
     * {
     *  debug: {
     *    apiCalls: '*'    // this will log ALL api calls
     *  }
     * }
     *
     * To log specific api calls:
     * {
     *  debug: {
     *    apiCalls: ['/echo'],
     *  }
     * }
     *
     * To suppress certain bodies on responses (because they're too noisy):
     * {
     *  debug: {
     *    apiCalls: '*',
     *    noBody: ['/some/really/noisy/path']
     *  }
     * }
     *
     * You can also use `noBody: '*'` to suppress all bodies from being displayed in the response debug log
     *
     * debugApiCall strips the query string, so your apiCalls should only be the actual endpoint. For example,
     * '/echo' if you want to debugApiCalls to '/echo?value=hi&status=277'
     *
     * @param path
     * @param source should be set to the class of the service making the API call. For example, with
     * AuthenticationService, your constructor should be:
     *    constructor(..., private api:ProfileService, ...) {
     *      api.source = this;
     *    }
     *
     * Otherwise, you'll end up with the source being: 'source not set'.
     *
     * @param path the url being called
     * @param httpMethod
     * @param body
     */
    debugApiCall(path: string, source: any, options: IRequestOptions): void;
    /**
     * Same as debugApiCall, but displays the API call response
     *
     * @param path the url that was called that led to the response
     * @param source
     * @param resp an @angular/http Response object
     */
    debugApiResponse(path: string, source: any, resp: Response): void;
    info(msg: any, ...parts: any[]): void;
    warn(msg: any, ...parts: any[]): void;
    error(err: Error | Response, msg?: string, ...parts: any[]): void;
    private logApiCall(path, source, options, type);
}
