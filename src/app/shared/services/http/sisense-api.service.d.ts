import { HttpClient } from '@angular/common/http';
import { AuthenticationStorageService } from '../authentication-storage.service';
import { LogService } from '../log.service';
import { ApiService } from './api.service';
export declare const SERVICE_AUDIENCE = "score.seedconnect.com";
export declare class SisenseApiService extends ApiService {
    constructor(authStorage: AuthenticationStorageService, http: HttpClient, log: LogService);
}
