import { HttpClient, HttpEvent, HttpParams, HttpResponse } from '@angular/common/http';
import { HttpObserve } from '@angular/common/http/src/client';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { AuthenticationToken } from '../../models/authentication-token';
import { AuthenticationStorageService } from '../authentication-storage.service';
import { LogService } from '../log.service';
/**
 * Developer note:
 *
 * The ApiService was originally implemented using the now legacy Http class (replaced by HttpClient). To
 * minimize the impact of the refactor, the ApiService has maintained its original interface. In the future
 * this may be refactored out to provide a more native experience.
 *
 */
export interface IApiServiceOptions {
    disableServerDown?: boolean;
}
/**
 * This interface replaces the deprecated https://angular.io/api/http/RequestOptionsArgs
 * Used in the get, post, etc methods
 */
export interface IRequestOptions {
    body?: any;
    headers?: {
        [key: string]: string;
    };
    observe?: HttpObserve;
    params?: HttpParams;
    method?: HttpMethod;
    reportProgress?: boolean;
    responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
    withCredentials?: boolean;
}
export declare type HttpMethod = 'DELETE' | 'GET' | 'HEAD' | 'JSONP' | 'OPTIONS' | 'POST' | 'PUT' | 'PATCH';
export declare type RequestResponse = Observable<ArrayBuffer> | Observable<Blob> | Observable<string> | Observable<Object> | Observable<any> | Observable<HttpEvent<ArrayBuffer>> | Observable<HttpEvent<Blob>> | Observable<HttpEvent<string>> | Observable<HttpEvent<any>> | Observable<HttpResponse<ArrayBuffer>> | Observable<HttpResponse<Blob>> | Observable<HttpResponse<string>> | Observable<HttpResponse<Object>>;
export declare abstract class ApiService {
    protected service: string;
    protected authStorage: AuthenticationStorageService;
    protected http: HttpClient;
    protected log: LogService;
    source: any;
    protected baseUrl: string;
    /**
     *
     * @param  service This is the name of the service used by the auth system. It is provided by the child class
     * when it calls super as is required when a child class implements ApiService.
     * @param  authStorage
     * @param  http
     * @param  log
     */
    constructor(service: string, authStorage: AuthenticationStorageService, http: HttpClient, log: LogService);
    private _authError$;
    /**
     * emitted when server returns a 401 (passes the authentication token that caused the 401, or null if non was found)
     */
    readonly authError$: Observable<AuthenticationToken>;
    private _serverDown$;
    /**
     * emitted when request returns a 0 (server is down)
     */
    readonly serverDown$: Observable<void>;
    request(endpoint: string, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<RequestResponse>;
    get(url: string, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
    post(url: string, body: any, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
    put(url: string, body: any, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
    delete(url: string, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
    patch(url: string, body: any, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
    head(url: string, options?: IRequestOptions, apiOptions?: IApiServiceOptions): Observable<any>;
}
