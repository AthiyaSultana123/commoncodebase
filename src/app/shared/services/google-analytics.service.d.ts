import { BrowserService } from './browser.service';
export declare class GoogleAnalyticsService {
    private browserService;
    private ga;
    private logToGoogle;
    constructor(browserService: BrowserService);
    error(description: string): void;
    pageView(newRoute: string): void;
    languageSearchEvent(eventData: {
        continent: string;
        country: string;
        language: string;
    }): void;
    projectSearchEvent(eventData: {
        continent: string;
        country: string;
        project: string;
        population: string;
        remainNeed: string;
        language: string;
    }): void;
    private debug(msg, ...parts);
}
