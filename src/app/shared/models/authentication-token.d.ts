import { ObjectId } from './object-id';
import { User } from './user';
export declare class AuthenticationToken {
    userId: ObjectId;
    email: string;
    phone: string;
    domain: string;
    firstName: string;
    lastName: string;
    issued: Date;
    expires: Date;
    audience: string;
    issuer: string;
    jwtId: string;
    jwtToken: string;
    key: string;
    constructor(userId: ObjectId, email: string, phone: string, domain: string, firstName: string, lastName: string, issued: Date, expires: Date, audience: string, issuer: string, jwtId: string, jwtToken: string, key: string);
    readonly userCreated: Date;
    readonly expired: boolean;
    static fromJson(json: any): AuthenticationToken;
    static fromJsonArray(jsons: any[]): AuthenticationToken[];
    /**
     * From a Json Web Token
     */
    static fromJwt(key: string, jwt: any): AuthenticationToken;
    /**
     * A token map is returned from the authentication authority:
     * {
     *    token: {
     *      "audience-id-1":"jwt_token-1",
     *      "audience-id-2":"jwt_token-2"
     *    }
     * }
     */
    static fromTokenMap(tokenMap: any): AuthenticationToken[];
    toUser(): User;
}
