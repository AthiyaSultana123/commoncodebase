export declare class ObjectId {
    private id;
    readonly timeStamp: Date;
    constructor(id: string);
    toString(): string;
}
