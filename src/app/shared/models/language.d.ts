import { ICartId } from './i-cart-id';
export declare class Language implements ICartId {
    static fromJson(json: any): Language;
    static fromSisenseEntry(headers: string[], values: any[]): Language;
    static fromSisenseResponseArray(json: any): Language[];
    readonly bannerImage: string;
    readonly cardImage: string;
    readonly buildImageFilename: string;
    readonly cartId: string;
    continent: string;
    country: string;
    latitude: number;
    longitude: number;
    languageName: string;
    languageId: string;
    campaignId: string;
    population: number;
    populationRange: string;
    beginYear: number;
    projectCount: number;
    orgCount: number;
    featuredLanguageFlag: boolean;
    isConfidentialLanguageFlag: boolean;
    isSignLanguageFlag: boolean;
    projectSensitivityLevel: number;
    primaryReligion: string;
    prayerCommitCount: number;
    commonFrameworkFlag: boolean;
    firstScriptureFlag: boolean;
    joshuaProjectLeastReachedFlag: boolean;
    completedFullBibleFlag: boolean;
    completedNewTestamentFlag: boolean;
    completedJesusFilmFlag: boolean;
    completedAudioRecordingsFlag: boolean;
    completed25ChaptersFlag: boolean;
    noTranslationYetFlag: boolean;
    translationInProgressFlag: boolean;
    translationTypeWrittenTranslationFlag: boolean;
    translationTypeWrittenStoriesFlag: boolean;
    translationTypeOralTranslationFlag: boolean;
    translationTypeOralStoryingFlag: boolean;
    translationTypeSignLanguageFlag: boolean;
    translationTypeFilmFlag: boolean;
    translationTypeOtherFlag: boolean;
    goalsFullBibleFlag: boolean;
    goalsFullNTFlag: boolean;
    goalsFullOTFlag: boolean;
    goalsAGospelFlag: boolean;
    goalsGenesisFlag: boolean;
    goalsNtPortionsFlag: boolean;
    goalsOtPortionsFlag: boolean;
    goalsJesusFilmFlag: boolean;
    goalsBibleStoriesFlag: boolean;
    langLifetimeFundingGoal: number;
    langLifetimeRemainNeed: number;
    beforeIBibleDonationsLangLifetimeRemainNeed: number;
    totalLanguagePledges: number;
    totalLanguageGiving: number;
    beforeIBibleTotalLanguageGiving: number;
    iBibleOnlyTotalRemainingNeed: number;
    imageSrc: string;
    lowResImageSrc: string;
    description: string;
    gathering2017Flag: boolean;
    visibleFlag?: boolean;
    region?: string;
    totalDonors?: number;
    copy(): Language;
    toJson(): string;
}
