import { ObjectId } from './object-id';
export declare class Address {
    address1: string;
    address2: string;
    city: string;
    country: string;
    state: string;
    zip: string;
    constructor();
    static fromJson(json: any): Address;
    static fromJsonArray(jsons: any[]): Address[];
}
export declare class BillingDetails {
    brand: string;
    expiresMonth: number;
    expiresYear: number;
    last4: string;
    name: string;
    static fromJson(json: any): BillingDetails;
}
export declare class User {
    id: ObjectId;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    mailingAddress?: Address;
    billingAddress?: Address;
    billingDetails?: BillingDetails;
    constructor();
    readonly fullName: string;
    readonly created: Date;
    static fromJson(json: any): User;
    static fromJsonArray(jsons: any[]): User[];
}
