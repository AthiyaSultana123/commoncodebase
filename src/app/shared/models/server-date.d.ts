export declare class ServerDate {
    static fromEpochSeconds(seconds: number): Date;
}
